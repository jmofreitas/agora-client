import {gql} from 'apollo-boost';

/*const getAuthorsQuery = gql`
    {
        authors{
            name
            id
        }
    }
`;

const getBooksQuery = gql`
    {
        books{
            name
            id
        }
    }
`;

const addBookMutation = gql`
    mutation($name:String!, $genre: String!, $authorId: ID!){
        addBook(name:$name, genre:$genre, authorId:$authorId){
            name
            id
        }
    }
`;*/

// CRUD TOPICS
const addTopicMutation = gql`
    mutation($user:ID!, $title:String!, $body:String!, $source:String!, $category:String!, $metaImage:String!, $tags:[TopicTagInput]){
        addTopic(user:$user, title:$title, body:$body, source:$source, category:$category, tags:$tags, metaImage:$metaImage){
            _id
            user
            title
            body
            source
            metaImage
            category
            tags {
                _id
                name
            }
            upvotes
            main
            community
        }
    }
`;

const updateTopicMutation = gql`
    mutation($body:String, $id: ID!){
        updateTopic(body:$body, _id:$id){
            _id
            body
        }
    }
`;

const deleteTopicMutation = gql`
    mutation($_id: ID!){
        deleteTopic(_id:$_id){
            _id
        }
    }
`;

const addPendingUserOnCommunity = gql`
    mutation($user: ID!, $community: ID!, $entryMessage: String!){
        addPendingUserOnCommunity(user:$user, community:$community, entry_message:$entryMessage){
            _id
            community
            entry_message
            status
        }
    }
`;

const getTopics = gql`
    {
        getTopics{
            _id
            title
            body
            user {
                _id
                firstName
                lastName
           }
           source
           metaImage
           category {
                _id
                name
           }
           tags {
                _id
                name
           }
           givenBadges{
                _id
                user {
                    _id
                    firstName
                    lastName
                }
                comment {
                    _id
                }
                topic {
                    _id
                    title
                    body
                }
                badge {
                    _id
                    name
                }
                date_given
                status
           }
           upvotes
           nrComments
           main
           community {
                _id
                name
                description
                number_members
                image
           }
        }
    }
`;

const getTopicById = gql`
    query($id: ID!){
        getTopicById(_id: $id){
            body
            title
            source
            metaImage
            community {
                _id
                name
                number_members
            }
            user{
                _id
                firstName
                image
            }
            upvotes
            nrComments
            givenBadges {
                _id
                badge {
                    _id
                    name
                    image
                }
            }
            tags {
               _id
               name
            }
            user{
                _id
                firstName
                lastName
            }
            comments {
                _id
                parent
                upvotes
                user {
                    _id
                    firstName
                    lastName
                }
                body
                created_at
                edited_at
                deleted_at
                children {
                    _id
                    parent
                    upvotes
                    user {
                        _id
                        firstName
                        lastName
                    }
                    body
                    created_at
                    edited_at
                    deleted_at
                    children {
                        _id
                        parent
                        upvotes
                        user {
                            _id
                            firstName
                            lastName
                        }
                        body
                        created_at
                        edited_at
                        deleted_at
                        children {
                            _id
                            parent
                            upvotes
                            user {
                                _id
                                firstName
                                lastName
                            }
                            body
                            created_at
                            edited_at
                            deleted_at
                            children {
                                _id
                                parent
                                upvotes
                                 user {
                                    _id
                                    firstName
                                    lastName
                                }
                                body
                                created_at
                                edited_at
                                deleted_at
                            }
                        }
                    }
                }
            } 
        }
    }
`;


// CRUD COMMENT

const getComment = gql`
    {
        id
        parent
        upvotes
        body
        created_at
        edited_at
        deleted_at
        children
    }
`;


const addCommentMutation = gql`
    mutation($parent: ID, $body:String!, $topic: ID!, $user: ID!){
        addComment(parent: $parent, body:$body, topic:$topic, user: $user){
            parent
            topic {
                _id
            }
            body
            user {
                _id
                firstName
            }
        }
    }
`;

const updateCommentMutation = gql`
    mutation($body:String!, $topic: ID!){
        updateComment(body:$body, topic:$topic){
            topic
            parent
            upvotes
            body
            created_at
            edited_at
            deleted_at
            children
        }
    }
`;

const deleteCommentMutation = gql`
    mutation($_id: ID!){
        deleteComment(_id: $_id){
            _id
            deleted_at
        }
    }
`;

// CRUD CATEGORY

const getCategories = gql`
    {
        getCategories{
            _id
            name
            image
        }
    }
`;

const addCategoryMutation = gql`
    mutation($name:String!, $description: String!, $image: String){
        addCategory(name:$name, description:$description, image: $image){
            _id
            name
            description
            image
        }
    }
`;

const updateCategoryMutation = gql`
    mutation($_id: ID!, $name: String, $description: String, $image: String){
        updateCategory(_id: $_id, name: $name, description: $description, image: $image){
            _id
            name
            description
            image
            edited_at
        }
    }
`;

const deleteCategoryMutation = gql`
    mutation($_id: ID!){
        deleteCategory(_id: $_id){
            _id
            deleted_at
        }
    }
`;


// CRUD ROLES
const addRoleMutation = gql`
    mutation($name:String!, $description: String!){
        addRole(name:$name, description:$description){
            _id
            name
            description
        }
    }
`;

// CRUD BADGES
const addBadgeMutation = gql`
    mutation($name:String!, $description: String!, $image: String){
        addBadge(name:$name, description:$description, image: $image){
            _id
            name
            description
            image
        }
    }
`;

// CRUD KARMA
const addKarmaMutation = gql`
    mutation($comments: Int, $posts: Int){
        addKarma(comments: $comments, posts: $posts){
            _id
            comments
            posts
        }
    }
`;

// CRUD ADD GIVEN BADGE

const addGivenBadgeMutation = gql`
    mutation($user: ID!, $comment: ID, $topic: ID, $badge: ID!, $attributed_by: ID!){
        addGivenBadge(user: $user, comment: $comment, topic: $topic, badge: $badge, attributed_by: $attributed_by){
            _id
            user
            comment
            topic
            badge
            attributed_by
        }
    }
`;

// CRUD COMMUNITY

const getCommunities = gql`
    {
        getCommunities{
            _id
            name
            description
            number_members
            category {
                _id
                name
            }
            image
            exampleMembers {
                _id
                user {
                    _id
                    firstName
                    lastName
                    image
                }
            }
        }
    }
`;

const getCommunityById = gql`
    query($id: ID!){
        getCommunityById(_id: $id){
            name
            description
            number_members
            category {
                _id
                name
            }
            image
            exampleMembers {
                _id
                user {
                    _id
                    firstName
                    lastName
                    image
                }
            }
        }
    }
`;

const checkUserOnCommunity = gql`
    query($userId: ID!, $communityId: ID!){
        checkUserOnCommunity(user: $userId, community: $communityId){
            _id
            user {
                _id
                firstName
            }
            community {
                _id
                name
            }
            status
            role {
                _id
                name
                description
            }
        }
    }
`;

const addCommunityMutation = gql`
    mutation($name: String!, $description: String!, $category: ID!, $image: String!){
        addCommunity(name: $name, description: $description, category: $category, image: $image){
            _id
            name
            description
            category{
               _id
               name
            }
            image
        }
    }
`;

const addAdminCommunity = gql`
    mutation($user: ID!, $community: ID!, $entry_message: String!){
        addAdminCommunity(user: $user, community: $community, entry_message: $entry_message){
            _id
        }
    }
`;

// USER
const getUserByName = gql`
    query($name: String!){
        getUserByName(firstName: $name){
            _id
            firstName
            lastName
            image
        }
    }
`;

const getUserById = gql`
    query($id: ID!){
        getUserById(_id: $id){
            firstName
            lastName
            email
            role {
                _id
                name
            }
            karma {
                _id
                comments
                topics
            }
            preferences {
                _id
            }
            nrComments
            receivedBadges {
                _id
                badge {
                    _id
                    name
                    image
                }
            }
            image
            userDescription
        }
    }
`;

const getPendingOnCommunity = gql`
    query($community: ID!){
        getPendingOnCommunity(community: $community){
            _id
            user {
                _id
                firstName
                lastName
                image
            }
            status
            entry_message
        }
    }
`;

const updatePendingUserOnCommunity = gql`
    mutation($_id: ID!, $status: Boolean!){
        updatePendingUserOnCommunity(_id: $_id, status: $status){
            _id
            status
        }
    }
`;

const getCommunitiesByUser = gql`
    query($user: ID!){
        getCommunitiesByUser(user: $user){
            community{
                _id
                name
                description
                number_members
                category {
                    _id
                    name
                }
                image
                exampleMembers {
                    _id
                    user {
                        _id
                        firstName
                        lastName
                        image
                    }
                }
            }
        }
    }
`;

// PREFERENCES //
const addPreferenceMutation = gql`
    mutation($category: ID!, $user: ID!){
        addPreference(category: $category, user: $user){
            _id
            user{
                _id
                firstName
            }
            category{
                _id
                name
            }
        }
    }
`;

// CONVERSATION //
const getConversationsWithUser = gql`
    query($user_1: ID!, $user_2: ID!){
        getConversationsWithUser(user_1: $user_1, user_2: $user_2){
            _id
            user_1
            user_2
            created_at
            edited_at
            message{
                _id
                from 
                to
                body
                timestamp
            }
        }
    }
`;

const getConversationsByUser = gql`
    query($user_1: ID!){
        getConversationsByUser(user_1: $user_1){
            _id
            user_1{
                _id
                firstName
                lastName
                image
            }
            user_2{
                _id
                firstName
                lastName
                image
            }
            created_at
            edited_at
            message{
                _id
                from 
                to
                body
                timestamp
            }
        }
    }
`;


const getConversationById = gql`
    query($_id: ID!){
        getConversationById(_id: $_id){
            _id
            user_1 {
                _id
                firstName
                lastName
            }
            user_2 {
                _id
                firstName
                lastName
            }
            created_at
            edited_at
        }
    }
`;

const addConversationMutation = gql`
    mutation($user_1: ID!, $user_2: ID!){
        addConversation(user_1: $user_1, user_2: $user_2){
            _id
            user_1{
                _id
                firstName
                lastName
            }
            user_2{
                _id
                firstName
                lastName
            }
        }
    }
`;

const getMessagesByConversation = gql`
    query($conversation: ID!, $skip: Int!){
        getMessagesByConversation(conversation: $conversation, skip: $skip){
            _id
            from
            to
            body
            timestamp
        }
    }
`;

const addMessageMutation = gql`
     mutation($from: ID!, $to: ID!, $body: String!, $conversation: ID!){
            addMessage(from: $from, to: $to, body: $body, conversation: $conversation){
                _id
                from
                to
                body
                timestamp
            }
        }
`;

//FAVORITES

const addFavourite = gql`
     mutation($user: ID!, $topic: ID!){
            addFavourite(user: $user, topic: $topic){
                _id
                user {
                    _id
                    firstName
                    lastName
                }
                topic {
                    _id
                    title
                    body
                }
            }
        }
`;

const checkFavourite = gql`
    query($user: ID!, $topic: ID!){
        checkFavourite(user: $user, topic: $topic){
            _id
            user{
                _id
                firstName
                lastName
            }
        }
    }
`;

const getFavouritesByUser = gql`
    query($user: ID!){
        getFavouritesByUser(user: $user){
            _id
            user{
                _id
                firstName
                lastName
            }
            topic{
                _id
                title
                body
                upvotes
                category{
                    _id
                    name
                    description
                }
            }
        }
    }
`;

const deleteFavourite = gql`
    mutation($_id: ID!){
        deleteFavourite(_id: $_id){
            _id
            deleted_at
        }
    }
`;

// UPVOTES

const addUpvote = gql`
     mutation($topic: ID, $comment: ID, $user_rewarded: ID!, $user_rewarding: ID!){
            addUpvote(topic: $topic, comment: $comment, user_rewarded: $user_rewarded, user_rewarding: $user_rewarding){
                _id
                user_rewarded {
                    _id
                    firstName
                    lastName
                }
                user_rewarding {
                    _id
                    firstName
                    lastName
                }
                topic {
                    _id
                    title
                    body
                }
                upvote
            }
        }
`;

const checkUpvote = gql`
    query($user: ID!, $topic: ID!){
        checkUpvote(user: $user, topic: $topic){
            _id
            user_rewarding{
                _id
                firstName
                lastName
            }
            user_rewarded{
                _id
                firstName
                lastName
            }
            upvote
        }
    }
`;

const deleteUpvote = gql`
    mutation($_id: ID!){
        deleteUpvote(_id: $_id){
            _id
            upvote
        }
    }
`;

// SEARCH

const getTagsBySearch = gql`
    query($name: String!){
        getTagsBySearch(name: $name){
            _id 
            name
        }
    }
`;

const getCategoriesBySearch = gql`
    query($name: String!){
        getCategoriesBySearch(name: $name){
            _id 
            name
        }
    }
`;

const getTopicsBySearch = gql`
    query($name: String!){
        getTopicsBySearch(title: $name){
            _id 
            title
            body
            category{
                _id
                name
            }
            upvotes
        }
    }
`;

const getCommunitiesBySearch = gql`
    query($name: String!){
        getCommunitiesBySearch(name: $name){
            _id 
            name
            description
            number_members
            category{
                _id
                name
            }
            exampleMembers {
                _id
                user {
                    _id
                    firstName
                    lastName
                    image
                }
            }
           image
        }
    }
`;

const getUsersBySearch = gql`
    query($name: String!){
        getUsersBySearch(firstName: $name){
            _id
            firstName
            lastName
            image
        }
    }
`;

// RECOMMENDED

const addUserSawTopic = gql`
    query($user: ID!, $topic: ID!){
        addUserSawTopic(user: $user, topic: $topic){
            topic
        }
    }
`;

const getTopicsByCategory = gql`
    query($category: String!){
        getTopicsByCategory(category: $category){
            _id
            title
            body
            metaImage
            userRequesting
            upvotes
            nrComments
            category{
                _id
                name
            }
        }
    }
`;

const getTopicsRecOrCat = gql`
    query($userRequesting: String!, $category: String!){
        getTopicsRecOrCat(userRequesting: $userRequesting, category: $category){
            _id
            title
            body
            metaImage
            userRequesting
           upvotes
           nrComments
           category{
            _id
            name
           }
        }
    }
`;

const getCommunitiesRec = gql`
    query($userRequesting: String!, $category: String!){
        getCommunitiesRec(userRequesting: $userRequesting, category: $category){
            _id 
            name
            description
            number_members
            category{
                _id
                name
            }
            exampleMembers {
                _id
                user {
                    _id
                    firstName
                    lastName
                    image
                }
            }
           image
        }
    }
`;


//RETIREI MENSAGENS DO ADDCONVERSATION

export {
    /*getAuthorsQuery,
    getBooksQuery,
    addBookMutation,*/
    getTopics,
    addTopicMutation,
    updateTopicMutation,
    deleteTopicMutation,
    getTopicById,
    getComment,
    addCommentMutation,
    updateCommentMutation,
    deleteCommentMutation,
    getCategories,
    addCategoryMutation,
    updateCategoryMutation,
    deleteCategoryMutation,
    addRoleMutation,
    addKarmaMutation,
    addGivenBadgeMutation,
    getCommunities,
    getCommunityById,
    checkUserOnCommunity,
    addCommunityMutation,
    addPendingUserOnCommunity,
    getUserById,
    getUserByName,
    addPreferenceMutation,
    addConversationMutation,
    getMessagesByConversation,
    getConversationById,
    addMessageMutation,
    getPendingOnCommunity,
    updatePendingUserOnCommunity,
    getCommunitiesByUser,
    getConversationsByUser,
    addFavourite,
    checkFavourite,
    deleteFavourite,
    getFavouritesByUser,
    addUpvote,
    checkUpvote,
    deleteUpvote,
    getTagsBySearch,
    getCategoriesBySearch,
    getTopicsBySearch,
    getCommunitiesBySearch,
    getCommunitiesRec,
    getUsersBySearch,
    addAdminCommunity,
    addUserSawTopic,
    getTopicsRecOrCat,
    getTopicsByCategory,
}