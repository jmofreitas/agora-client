import React from "react";
import {useQuery} from "@apollo/react-hooks";
import {getTopics, getTopic, addTopicMutation, updateTopicMutation, deleteTopicMutation} from "./queries";

function TestQueries() {
    const {loading, error, data} = useQuery(getTopics);

    const requestLogsDisplay = () => {
        //* getTopic Query
        // if (loading) return <p>Loading ...</p>;
        // console.log(data);
        //     return (
        //         <li key={data.topic.id}>{data.topic.body}</li>
        //     )

        // getTopics Query
        if (loading) return <p>Loading ...</p>;
        return data ? data.topics.map(topic => {
            return (
                <li key={topic.id}>{topic.name}</li>
            )
        }) : [];

    }

    return(
        <div>
            <ul>
                {requestLogsDisplay()}
            </ul>
        </div>
    )
}

export default TestQueries;