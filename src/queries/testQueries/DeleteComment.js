import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {
    addBadgeMutation,
    addCategoryMutation,
    addTopicMutation,
    deleteCommentMutation,
    getTopics,
    updateCategoryMutation
} from "../queries";
function DeleteComment(props) {
    const [name, setName] = useState('');
    const [deleteComment, {dataAdded}] = useMutation(deleteCommentMutation);

    const submitForm = (e) => {
        e.preventDefault();
        deleteComment({
            variables: {
                _id: '5e177ae6076a1f300c7c4d5'
            },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Comment name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default DeleteComment;