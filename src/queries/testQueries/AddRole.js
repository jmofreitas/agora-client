import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addCategoryMutation, addRoleMutation, addTopicMutation, getTopics} from "../queries";

function AddRole(props) {
    const [name, setName] = useState('');
    const [addRole, {dataAdded}] = useMutation(addRoleMutation);

    const submitForm = (e) => {
        e.preventDefault();
        addRole({
            variables: {
                name: "NOME DO ROLE",
                description: "kek"
                },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Role name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default AddRole;