import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useQuery, useMutation} from '@apollo/react-hooks'
import {getAuthorsQuery, addBookMutation, getBooksQuery} from "../queries";

function AddBook(props) {
    const [name, setName] = useState('');
    const [genre, setGenre] = useState('');
    const [authorId, setAuthorId] = useState('');

    const {loading, error, data} = useQuery(getAuthorsQuery);
    const [addBook, {dataAdded}] = useMutation(addBookMutation);

    const displayAuthors = () => {
        if (loading) return <p>Loading ...</p>;
        return data.authors.map(author => {
            return (
                <option key={author.id} value={author.id}>{author.name}</option>
            )
        })
    };

    const submitForm = (e) => {
        e.preventDefault();
        addBook({
            variables: {
                name,
                genre,
                authorId
            },
            refetchQueries:[
                {query: getBooksQuery}
            ]
        });
        console.log(name, genre, authorId);
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Book name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>

            <div className='field'>
                <label>Genre:</label>
                <input type='text' onChange={(event => {
                    setGenre(event.target.value)
                })}/>
            </div>

            <div className='field'>
                <label>Author:</label>
                <select onChange={(event => {
                    setAuthorId(event.target.value)
                })}>
                    <option>Select author</option>
                    {displayAuthors()}
                </select>
            </div>

            <button>+</button>
        </form>
    )

}

export default AddBook;