import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addBadgeMutation, addCategoryMutation, addTopicMutation, getTopics} from "../queries";

function AddBadge(props) {
    const [name, setName] = useState('');
    const [addBadge, {dataAdded}] = useMutation(addBadgeMutation);

    const submitForm = (e) => {
        e.preventDefault();
        addBadge({
            variables: {
                name: "TESTEzxczxc",
                description: "ITESTE###"
            },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Badge name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default AddBadge;