import React, {useEffect, useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addCommentMutation, getTopic} from "../queries";

function AddComment(props) {
    const [body, setBody] = useState('');
    const [addComment, {dataAdded}] = useMutation(addCommentMutation);

    const submitForm = (e) => {
        e.preventDefault();
        addComment({
            variables: {
                body,
                topic: '5de43f906ab19228d535c255'
            },
            refetchQueries:[
                {query: getTopic,
                    variables: { id: '5de43f906ab19228d535c255' },
                }
            ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Comment:</label>
                <input type='text' onChange={(event => {
                    setBody(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default AddComment;