import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addTopicMutation, getTopics} from "../queries";

function AddTopic(props) {
    const [name, setName] = useState('');
    const [addTopic, {dataAdded}] = useMutation(addTopicMutation);

    const submitForm = (e) => {
        e.preventDefault();
        addTopic({
            variables: {
                _id: 1,
                user: "x",
                title: "x2",
                body: name,
                category: "Desporto",
                tags: [{
                    id: "5e177f941c9d440000031c2f",
                    name: "cenas"
                }]
            },
            refetchQueries:[
                {query: getTopics}
            ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Topic name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default AddTopic;