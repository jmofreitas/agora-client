import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addCategoryMutation, addTopicMutation, getTopics} from "../queries";

function AddCategory(props) {
    const [name, setName] = useState('');
    const [addCategory, {dataAdded}] = useMutation(addCategoryMutation);

    const submitForm = (e) => {
        e.preventDefault();
        addCategory({
            variables: {
                name: "TESTEzxczxc",
                description: "ITESTE###"
                },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Category name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default AddCategory;