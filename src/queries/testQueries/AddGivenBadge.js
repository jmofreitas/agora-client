import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addBadgeMutation, addCategoryMutation, addGivenBadgeMutation, addTopicMutation, getTopics} from "../queries";

function AddGivenBadge(props) {
    const [name, setName] = useState('');
    const [addGivenBadge, {dataAdded}] = useMutation(addGivenBadgeMutation);

    const submitForm = (e) => {
        e.preventDefault();
        addGivenBadge({
            variables: {
                user: '5e1e2e9d5b9e883cbc009e4c',
                topic: '5e1a0ccd6f055a4530144959',
                badge: '5e2b3f59e158c643e6c30838',
                attributed_by: '5e2a219a391be4183855aea0'
            },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Given Badge name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default AddGivenBadge;