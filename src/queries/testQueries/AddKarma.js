import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addCategoryMutation, addKarmaMutation, addTopicMutation, getTopics} from "../queries";

function AddKarma(props) {
    const [name, setName] = useState('');
    const [addKarma, {dataAdded}] = useMutation(addKarmaMutation);

    const submitForm = (e) => {
        e.preventDefault();
        addKarma({
            variables: {
                comments: 10,
                posts: 10
                },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Karma name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default AddKarma;