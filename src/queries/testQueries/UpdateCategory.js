import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {addBadgeMutation, addCategoryMutation, addTopicMutation, getTopics, updateCategoryMutation} from "../queries";

function UpdateCategory(props) {
    const [name, setName] = useState('');
    const [updateCategory, {dataAdded}] = useMutation(updateCategoryMutation);

    const submitForm = (e) => {
        e.preventDefault();
        updateCategory({
            variables: {
                _id: '5e2b34cc7f4d413b0ea0d5ba',
                name: "updated_name",
                description: "updated_desc",
            },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Category name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default UpdateCategory;