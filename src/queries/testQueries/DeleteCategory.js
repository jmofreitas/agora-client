import React, {useState} from 'react';
import {gql} from 'apollo-boost';
import {useMutation} from '@apollo/react-hooks'
import {
    addBadgeMutation,
    addCategoryMutation,
    addTopicMutation, deleteCategoryMutation,
    deleteCommentMutation,
    getTopics,
    updateCategoryMutation
} from "../queries";
function DeleteCategory(props) {
    const [name, setName] = useState('');
    const [deleteCategory, {dataAdded}] = useMutation(deleteCategoryMutation);

    const submitForm = (e) => {
        e.preventDefault();
        deleteCategory({
            variables: {
                _id: '5e2b34cc7f4d413b0ea0d5ba'
            },
            // refetchQueries:[
            //     {query: getTopics}
            // ]
        });
    };


    return (
        <form id='add-book' onSubmit={submitForm}>
            <div className='field'>
                <label>Category name:</label>
                <input type='text' onChange={(event => {
                    setName(event.target.value)
                })}/>
            </div>
            <button>+</button>
        </form>
    )

}

export default DeleteCategory;