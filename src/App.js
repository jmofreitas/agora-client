import React, {useEffect, useState} from 'react';
import './css/main.min.css';
import {ApolloClient, ApolloLink, InMemoryCache, HttpLink} from 'apollo-boost';
import {onError} from "apollo-link-error";
import MessageList from './components/Messages/MessageList';
import {ApolloProvider} from 'react-apollo'
import Grid from "@material-ui/core/Grid/Grid";
import Home from "./components/home/Home";
import MainScreen from "./components/MainScreen";
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import SignInSide from "./components/SignInSide";
import SignUp from "./components/SignUp";
import {BrowserRouter, Redirect} from "react-router-dom";
import CreateDiscussion from "./components/Discussion/CreateDiscussion";
import Discussion from "./components/Discussion/Discussion";
import SearchMenu from "./components/Search/SearchMenu";
import Notifications from "./components/Notifications/Notifications";
import {useCookies} from "react-cookie";
import VerificationMenuBar from "./components/VerificationMenuBar";
import SettingsScreen from "./components/Settings/SettingsScreen";
import ProfilePage from "./components/Profile/ProfilePage";
import Community from "./components/Community/Community";
import MessageConversation from "./components/Messages/MessageConversation";
import CreateCommunity from "./components/Community/CreateCommunity";
import CinemaPage from "./components/CinemaPage/CinemaPage";
import Categories from "./components/Categories";
import FirstSelect from "./components/home/FirstSelect";
import TokenDecode from "./utilComponents/TokenDecode";
import Feed from "./components/home/Feed";
import {GlobalStateProvider} from "./components/Context/useGlobalState";
import Box from "@material-ui/core/Box/Box";
import ChangeMail from "./components/Settings/ChangeMail";
import About from "./components/Settings/About";


const Switch = require("react-router-dom").Switch;
const Route = require("react-router-dom").Route;

const httpLink = new HttpLink({uri: 'http://agora.jmfreitas.com/graphql'});


const authLink = new ApolloLink((operation, forward) => {
    // Retrieve the authorization token from local storage.
    const token = localStorage.getItem('token');
    // Use the setContext method to set the HTTP headers.
    operation.setContext({
        headers: {
            authorization: token ? `${token}` : ''
        }
    });

    // Call the next link in the middleware chain.
    return forward(operation);
});

// const link = onError(({graphQLErrors, networkError}) => {
//     if (graphQLErrors)
//         graphQLErrors.forEach(({message, locations, path}) =>
//             console.log(
//                 `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
//             )
//         );
//     if (networkError) console.log(`[Network error]: ${networkError}`);
// });

const client = new ApolloClient({
    link: authLink.concat(httpLink), // Chain it with the HttpLink
    cache: new InMemoryCache(),
});

const theme = createMuiTheme({
    overrides: {
        MuiButton: {
            textPrimary: {
                color: '#fff',
            }
        },
    },
});

function App() {

    const [cookies] = useCookies(['token', 'preferences']);
    const [tokenUndefined, setTokenUndefined] = useState(true);

    const decoded = TokenDecode();

    useEffect(() => {
        if (cookies.token) {
            setTokenUndefined(tokenIsUndefined());
        }
    }, [cookies]);

    const tokenIsUndefined = () => {
        return cookies.token === "undefined" || !cookies.token;
    };


    if (!decoded && !tokenIsUndefined()) {
        window.location.reload();
    }

    return (
        <ApolloProvider client={client}>
            <GlobalStateProvider>
                <ThemeProvider theme={theme}>
                    <BrowserRouter>
                        {(tokenIsUndefined() && window.location.pathname !== "/register") && <Redirect to='/login'/>}
                        <Grid container justify="center">

                            <Grid item xs={12} md={10} sm={10} lg={8}>
                                <div id="main" style={{marginBottom: 60, marginTop:70}}>
                                    {/*<Home/>*/}
                                    <Switch>
                                        {/*<Box boxShadow={2} style={{minHeight: "88vh"}}>*/}

                                            {tokenIsUndefined() &&
                                            <Route exact path='/login' component={SignInSide}/>
                                            }
                                            {tokenIsUndefined() ?
                                                <Route exact path='/register' component={SignUp}/>
                                                :
                                                <div>
                                                    <Route exact path='/login' component={SignInSide}/>
                                                    <Route exact path='/register' component={SignUp}/>
                                                    {cookies.preferences === 'true' &&
                                                    <Route exact path='/' component={() => <Feed/>}/>
                                                    }
                                                    {cookies.preferences === 'false' &&
                                                    <Route exact path='/' component={() => <FirstSelect/>}/>
                                                    }
                                                    <Route exact path='/search' component={SearchMenu}/>
                                                    <Route exact path='/create' component={CreateDiscussion}/>
                                                    <Route exact path='/notifications' component={Notifications}/>
                                                    <Route exact path='/messages' component={MessageList}/>
                                                    <Route path='/messages/:id' component={MessageConversation}/>
                                                    {/*<Route exact path='/messages/:id' component={MessageConversation}/>  USE THIS WHEN TOKEN WITH ID'S ARE DONE*/}
                                                    <Route exact path='/cinema' component={CinemaPage}/>
                                                    <Route exact path='/discussion/:id' component={Discussion}/>
                                                    <Route exact path='/changeMail/' component={ChangeMail}/>
                                                    <Route exact path='/about/' component={About}/>
                                                    <Route exact path='/category/:id' component={Categories}/>
                                                    <Route exact path='/settings' component={SettingsScreen}/>
                                                    <Route path='/profile/:id' component={ProfilePage}/>
                                                    {<Route exact path='/community/:id' component={Community}/>}
                                                    <Route exact path='/newcommunity' component={CreateCommunity}/>
                                                </div>
                                            }
                                        {/*</Box>*/}
                                    </Switch>
                                    {(!tokenIsUndefined() && decoded) &&
                                    <VerificationMenuBar token={decoded}/>
                                    }
                                    {/*<FirstSelect/>*/}
                                </div>
                            </Grid>
                        </Grid>
                    </BrowserRouter>
                </ThemeProvider>
            </GlobalStateProvider>
        </ApolloProvider>
    );
}

export default App;
