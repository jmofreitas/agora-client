import React from 'react';
import ListItem from "@material-ui/core/ListItem/ListItem";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List/List";
import {makeStyles} from "@material-ui/core";
import {useCookies} from "react-cookie";
import {useHistory} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
    },
    principal: {
        backgroundColor: '#e8e8e8',
    },

    secondary: {
        borderBottomStyle: 'solid',
        borderBottomWidth: 'thin',
        borderBottomColor: '#e8e8e8',
        height: '3rem',
        alignItems: 'center',
    },

    dialog: {

        flex: '0 0 auto',
        display: 'flex',
        padding: '8px',
        alignItems: 'center',
        justifyContent: 'flex-end',
        margin: 'auto',

    },

}));

function SettingsScreen() {
    const classes = useStyles();
    const [cookies, removeCookie] = useCookies(['token']);
    const history = useHistory();

    const logout = () => {
        removeCookie("token");
        history.push("/login");
    };

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (

        <div className={classes.root}>
            <List>
                <ListItem alignItems="flex-start" className={classes.principal}>
                    <Typography variant="h6">Conta</Typography>
                </ListItem>

                <ListItem alignItems="flex-start" className={classes.secondary}>
                    <Typography onClick={() => {
                        history.push("/changeMail");
                    }} variant="body1">Alterar e-mail e palavra passe</Typography>
                </ListItem>

                <ListItem alignItems="flex-start" className={classes.secondary}>
                    <Typography onClick={handleClickOpen} variant="body1">Alterar foto de perfil</Typography>
                </ListItem>

                <ListItem alignItems="flex-start" className={classes.principal}>
                    <Typography variant="h6">Geral</Typography>
                </ListItem>

                <ListItem alignItems="flex-start" className={classes.secondary}>
                    <Typography variant="body1">Política de Privacidade</Typography>
                </ListItem>

                <ListItem alignItems="flex-start" className={classes.secondary}>
                    <Typography onClick={() => {
                        history.push("/about");
                    }} variant="body1">Sobre</Typography>
                </ListItem>

                <ListItem alignItems="flex-start" style={{marginTop: '1rem'}} button onClick={() => {
                    logout()
                }}>
                    <Typography variant="body1" style={{marginLeft: 0, color: '#D10019'}}>Terminar sessão</Typography>
                </ListItem>
            </List>

            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Alterar foto de perfil"}</DialogTitle>

                <AddCircleRoundedIcon fontSize="large" style={{
                    width: '100px', marginLeft: 'auto', marginRight: 'auto', display: 'flex',
                    alignItems: 'center'
                }} variant="contained" className="redBackgrounds"
                    // onClick={() => { submitForm()}} disabled={btnDisabled}
                >

                </AddCircleRoundedIcon>


                <DialogActions className={classes.dialog}>
                    <Button onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Button onClick={handleClose} autoFocus>
                        Concluir
                    </Button>
                </DialogActions>
            </Dialog>


        </div>


    )
}

export default SettingsScreen;
