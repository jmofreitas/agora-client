import React from 'react';
import Typography from "@material-ui/core/Typography/Typography";
import imgsPublico from "../../imgs/publico.png";



const styles = {

    root: {
        padding: '1rem',
        height: '100%',
        border: 'none',
        boxShadow: 'none'
    },
};

function About() {

    return(


        <div style={styles.root}>

            <Typography  variant="h6">Sobre</Typography>

            <Typography style={{paddingBottom: '1rem',paddingTop: '1rem'}}  variant="body2">

                O projeto ÁGORA pode definir-se como um fórum de discussão entre leitores e assinantes do Jornal Público,
                cujo objetivo é fomentar a interação entre leitores e assinantes do jornal através da existência de um mecanismo
                de recomendação, numa perspetiva de comunidade.
                Esta aplicação cruza interesses e categorias dos utilizadores padrões de utilização semelhantes.

                <img src={imgsPublico} style={{padding: '2rem', textAlign: 'center',
                    display: 'block', marginLeft: 'auto', marginRight: 'auto', width: '50%'}}/>

            </Typography>

        </div>

    )




}
export default About;
