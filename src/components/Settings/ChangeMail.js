import React, {useEffect, useRef, useState} from 'react';
import TextField from '@material-ui/core/TextField';
import {makeStyles, withStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography/Typography";
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid/Grid";

const useStyles = makeStyles(() => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: '1rem',
        height: '100%',
    },
}));

const ColorButton = withStyles(theme => ({
    root: {
        color: theme.palette.getContrastText('#D10019'),
        backgroundColor: '#D10019',
        '&:hover': {
            backgroundColor: '#D10019',
        },
    },
}))(Button);

function ChangeMail() {

    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid container>
                    <Grid item xs={12} md={6}>
                        <Typography variant="h6">Alterar e-mail</Typography>
                        <form style={{paddingBottom: '1rem'}} noValidate autoComplete="off">
                            <TextField variant="outlined"
                                       margin="normal" fullWidth id="standard-secondary"/>
                        </form>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12} md={6}>
                        <Typography variant="body1">Confirmar e-mail</Typography>
                        <form style={{paddingBottom: '1rem'}} noValidate autoComplete="off">
                            <TextField variant="outlined"
                                       margin="normal" fullWidth id="standard-secondary"/>
                        </form>
                    </Grid>
                </Grid>
                <Grid container justify="center">
                    <Grid item>
                        <ColorButton>Confirmar</ColorButton>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container style={{paddingTop: '1rem'}}>
                <Grid container>
                    <Grid item xs={12} md={6}>
                        <Typography variant="h6">Alterar palavra-passe</Typography>
                        <form style={{paddingBottom: '1rem'}} noValidate autoComplete="off">
                            <TextField variant="outlined"
                                       margin="normal" fullWidth id="standard-secondary"/>
                        </form>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12} md={6}>
                        <Typography variant="body1">Confirmar palavra-passe</Typography>
                        <form style={{paddingBottom: '1rem'}} noValidate autoComplete="off">
                            <TextField variant="outlined"
                                       margin="normal" fullWidth id="standard-secondary"/>
                        </form>
                    </Grid>
                </Grid>
                <Grid container justify="center">
                    <Grid item>
                        <ColorButton>
                            Confirmar
                        </ColorButton>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default ChangeMail;