import React, {useEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import NotificationsNoneOutlinedIcon from '@material-ui/icons/NotificationsNoneOutlined';
import ForumOutlinedIcon from '@material-ui/icons/ForumOutlined';
import {Grid} from '@material-ui/core';
import Hidden from '@material-ui/core/Hidden';
import {fade, makeStyles} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import {useHistory, useLocation} from 'react-router-dom';
import TokenDecode from '../utilComponents/TokenDecode';
import ImgsAgora from '../../src/imgs/AGORAlogo.png';
import Avatar from "@material-ui/core/Avatar";
import LoadingComponent from "./styleComponents/LoadingComponent";
import {getUserById} from "../queries/queries";
import {useQuery} from "@apollo/react-hooks";
import SettingsIcon from '@material-ui/icons/Settings';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';


const useStyles = makeStyles(theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
}));

function MenuBar() {
    const classes = useStyles();
    const history = useHistory();
    const location = useLocation();
    const decoded = TokenDecode();
    const [auth] = React.useState(true);
    const [content, setContent] = useState([]);
    const [value, setValue] = useState('home');

    const {loading: loading, error, data} = useQuery(getUserById, {
        variables: {
            id: decoded._id
        }
    });

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleMenu = () => {
        if (decoded) {
            history.push("/profile/" + decoded._id)
        }
    };

    const displayProfileImg = () => {
        if (loading) return <LoadingComponent/>;
        setContent(data.getUserById);
    };

    useEffect(() => {
        displayProfileImg()
    }, [loading]);


    return (
        <AppBar value={value} className="menuColor" onChange={handleChange} style={{
            width: '100%',
            backgroundColor: 'white',
            position: 'fixed',
            top: 0,
            left: 0,
            height: 60,
            zIndex: 5000,
            boxShadow: '0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12)',
        }}>
            <Toolbar>
                <Grid container justify="center">
                    <Grid container item xs={12} md={10} sm={10} lg={8} alignItems="center">
                        <div className="menuTitle" onClick={() => {
                            history.push("/");
                        }}>
                            <Hidden mdUp>
                            <img src={ImgsAgora} style={{width: '20%'}}/>
                            </Hidden>
                            <Hidden smDown>
                                <img src={ImgsAgora} style={{width: '10%'}}/>
                            </Hidden>
                        </div>
                        <Hidden smDown>
                            <div>
                                <IconButton color="inherit" onClick={() => {
                                    history.push("/search");
                                }}>
                                    <SearchIcon/>
                                </IconButton>
                                <IconButton color="inherit" onClick={() => {
                                    history.push("/create");
                                }}>
                                    <AddCircleOutlineIcon/>
                                </IconButton>
                                <IconButton color="inherit" onClick={() => {
                                    history.push("/messages");
                                }}>
                                    <ForumOutlinedIcon/>
                                </IconButton>
                                <IconButton color="inherit" onClick={() => {
                                    history.push("/notifications");
                                }}>
                                    <NotificationsNoneOutlinedIcon/>
                                </IconButton>
                            </div>
                        </Hidden>
                        {auth ? (
                                <div>
                                    {location.pathname.startsWith("/profile/") ?
                                        <IconButton color="inherit" onClick={() => {
                                            history.push("/settings")
                                        }}>
                                            <SettingsIcon/>
                                        </IconButton> :
                                        <IconButton
                                            aria-label="account of current user"
                                            aria-controls="menu-appbar"
                                            aria-haspopup="true"
                                            onClick={handleMenu}
                                            color="inherit"
                                        >
                                            {content.image ?
                                                <Avatar src={content.image} className={classes.small}></Avatar>
                                                : <AccountCircle/>}
                                        </IconButton>}
                                </div>
                            ) :
                            <div>
                                <Typography variant="h6" className="menuTitle">
                                    LOGIN
                                </Typography>
                            </div>}
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}

export default MenuBar;