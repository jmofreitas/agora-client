import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ImgProfile from "../../imgs/mlemtest.jpg";
import {Card, CardContent} from "@material-ui/core";
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import FeaturedPlayListOutlinedIcon from '@material-ui/icons/FeaturedPlayListOutlined';
import BookmarkBorderSharpIcon from '@material-ui/icons/BookmarkBorderSharp';
import {useHistory, useParams} from 'react-router-dom';
import {
    addFavourite,
    addUpvote,
    addUserSawTopic,
    checkFavourite,
    checkUpvote,
    deleteFavourite,
    deleteUpvote,
    getTopicById
} from "../../queries/queries";
import {useMutation, useQuery} from "@apollo/react-hooks";
import LoadingComponent from "../styleComponents/LoadingComponent";
import Comments from "./Comments";
import Divider from "@material-ui/core/Divider/Divider";
import BookmarkIcon from '@material-ui/icons/Bookmark';
import TokenDecode from "../../utilComponents/TokenDecode";
import Chip from "@material-ui/core/Chip/Chip";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 3),
    },
    padding: {
        padding: theme.spacing(2),
    },
    paper: {
        maxWidth: 400,
        padding: theme.spacing(1),
        boxShadow: 'none',
        display: "flex"
    },

    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },

    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },

    margin: {
        margin: theme.spacing(1),

    },

    cover: {
        width: '100%',
        maxHeight: '70%',
        borderRadius: '6px',
    },

    card: {
        boxShadow: 'none',
        border: 'none',
    },

    comment: {
        paddingBottom: '1rem',
    },
}));

function Discussion(props) {
    const classes = useStyles();
    const history = useHistory();
    const [content, setContent] = useState(0);
    const [profileImgToShow, setProfileImgToShow] = useState(ImgProfile);
    const [topicImgToShow, setTopicImgToShow] = useState(ImgProfile);
    const [favouriteID, setFavouriteID] = useState('');
    const [upvoteID, setUpvoteID] = useState('');
    const [isFavourite, setIsFavourite] = useState(false);
    const [isUpvoted, setIsUpvoted] = useState(false);
    const [favourite, {data: dataFavourite}] = useMutation(addFavourite);
    const [upvote, {data: dataUpvote}] = useMutation(addUpvote);

    const [timeoutCheck, setTimeoutCheck] = useState(false);
    const [delFavourite, {data: dataDelFavourite}] = useMutation(deleteFavourite);
    const [delUpvote, {data: dataDelUpvote}] = useMutation(deleteUpvote);
    const {id} = useParams();
    const decoded = TokenDecode();

    const {loading: loadingSaw, error: errorSaw, data: dataUserSawTopic} = useQuery(addUserSawTopic,
        {
            skip: !timeoutCheck,
            variables: {
                user: decoded._id,
                topic: id,
            }
        });

    const {loading, error, data} = useQuery(getTopicById, {
        variables: {
            id: id
        }
    });

    const {loading: loadingCheck, error: errorCheck, data: dataCheck} = useQuery(checkFavourite, {
        skip: decoded._id === null,
        variables: {
            user: decoded._id,
            topic: id
        }
    });

    const {loading: loadingCheckU, error: errorCheckU, data: dataCheckU} = useQuery(checkUpvote, {
        skip: decoded._id === null,
        variables: {
            user: decoded._id,
            topic: id
        }
    });


    const displayDiscussion = () => {
        if (loading) return <LoadingComponent/>;
        setContent(data.getTopicById);
    };


    const profileImg = () => {
        if (loading) return <LoadingComponent/>;
        if (data.getTopicById.user[0].metaImage) {
            setProfileImgToShow(data.getTopicById.user[0].metaImage);
        } else {
            setProfileImgToShow(ImgProfile);
        }
    };

    const topicImg = () => {
        if (loading) return <LoadingComponent/>;
        if (data.getTopicById.metaImage) {
            setTopicImgToShow(data.getTopicById.metaImage);
        } else {
            setTopicImgToShow(ImgProfile);
        }
    };

    const addBookmark = (status) => {
        setIsFavourite(status);
        if (status) {
            favourite({
                variables: {
                    user: decoded._id,
                    topic: id
                },
                refetchQueries: [
                    {
                        query: checkFavourite,
                        variables: {
                            user: decoded._id,
                            topic: id
                        }
                    },
                    {
                        query: getTopicById,
                        variables: {
                            id: id
                        }
                    }
                ]

            });
        } else {
            delFavourite({
                variables: {
                    _id: favouriteID
                },
                refetchQueries: [
                    {
                        query: checkFavourite,
                        variables: {
                            user: decoded._id,
                            topic: id
                        }
                    },
                    {
                        query: getTopicById,
                        variables: {
                            id: id
                        }
                    }
                ]
            })
        }
    };

    const addUpvoteFn = (status) => {
        setIsUpvoted(status);
        if (status) {
            upvote({

                variables: {
                    user_rewarding: decoded._id,
                    user_rewarded: data.getTopicById.user[0]._id,
                    topic: id
                },
                refetchQueries: [
                    {
                        query: checkUpvote,
                        variables: {
                            user: decoded._id,
                            topic: id
                        }
                    },
                    {
                        query: getTopicById,
                        variables: {
                            id: id
                        }
                    }
                ]

            });
        } else {
            delUpvote({
                variables: {
                    _id: upvoteID
                },
                refetchQueries: [
                    {
                        query: checkUpvote,
                        variables: {
                            user: decoded._id,
                            topic: id
                        }
                    },
                    {
                        query: getTopicById,
                        variables: {
                            id: id
                        }
                    }
                ]
            })
        }
    };

    useEffect(() => {
        displayDiscussion();
        profileImg();
        topicImg();
    }, [loading]);

    useEffect(() => {
        const cenas = setTimeout(() => {
            setTimeoutCheck(true);
        }, 10000);
        return (() => {
            clearTimeout(cenas);
        })
    }, []);

    useEffect(() => {
        if (dataCheck) {
            if (dataCheck.checkFavourite.length > 0) {
                setIsFavourite(true);
                setFavouriteID(dataCheck.checkFavourite[0]._id)
            }
        }

        if (dataCheckU) {
            if (dataCheckU.checkUpvote.length > 0) {
                if (dataCheckU.checkUpvote[0].upvote === true) {
                    setIsUpvoted(true);
                }
                setUpvoteID(dataCheckU.checkUpvote[0]._id)
            }
        }
    }, [dataCheck, dataCheckU]);

    if (loading) return <LoadingComponent/>;

    console.log(content);
    return (
        <Grid className={classes.root}>
            <Grid>
                <Card className={classes.card} variant="outlined">
                    {data.getTopicById &&
                    <CardContent>
                        <Grid container onClick={() => {history.push(`/profile/${data.getTopicById.user[0]._id}`)}}>
                            <Grid item>
                                <Avatar src={data.getTopicById.user[0].image} className={classes.small}/>
                            </Grid>
                            <Grid item style={{marginLeft: 4}}>
                                <Typography variant="button" noWrap>
                                    {data.getTopicById.user[0].firstName} {data.getTopicById.user[0].lastName}
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container style={{paddingTop: '1rem'}}>
                            <Typography variant="h5">
                                {data.getTopicById.title}
                            </Typography>
                        </Grid>
                        <Grid container style={{paddingTop: '1rem'}}>
                            <Typography variant="body1">{data.getTopicById.body}</Typography>
                        </Grid>
                        <Grid container style={{
                            marginTop: '1rem',
                        }}>
                            <img alt="profile" className={classes.cover} src={topicImgToShow}
                                 style={{width: '360px'}}/>
                        </Grid>
                        <Grid container style={{paddingTop: '1rem'}}>
                            {data.getTopicById.tags.length > 0 && data.getTopicById.tags.map((tags, key) => (
                                <Chip style={{marginTop: '2%', marginRight: '5px'}} label={`#${tags.name}`}
                                      className="tags"
                                      key={`tag${key}`}/>
                            ))
                            }
                        </Grid>
                        <Grid container spacing={1} style={{paddingTop: '1rem'}}>
                            <Tooltip title="Likes" aria-label="add">
                                <Grid item>
                                    <Button style={{color: '#787777'}}>
                                        {isUpvoted ?
                                            <ThumbUpAltIcon fontSize="small"
                                                            style={{marginRight: '5px', color: '#d10019'}}
                                                            onClick={() => {
                                                                addUpvoteFn(false)
                                                            }
                                                            }/>
                                            : <ThumbUpAltOutlinedIcon fontSize="small" style={{marginRight: '5px'}}
                                                                      onClick={() => {
                                                                          addUpvoteFn(true)
                                                                      }
                                                                      }/>
                                        }
                                        {data.getTopicById.upvotes}
                                    </Button>
                                </Grid>
                            </Tooltip>
                            <Grid item>
                                <Tooltip title="Comentários" aria-label="add">
                                    <Button style={{color: '#787777'}}>
                                        <ChatOutlinedIcon fontSize="small"
                                                          style={{marginRight: '5px'}}/>{data.getTopicById.nrComments}
                                    </Button>
                                </Tooltip>
                            </Grid>
                            {data.getTopicById.source &&
                            <Tooltip title="Notícia Público" aria-label="add">

                                <Grid item>
                                    <Button href={data.getTopicById.source} target="_blank" style={{color: '#787777'}}>
                                        <FeaturedPlayListOutlinedIcon fontSize="small" style={{marginRight: '5px'}}/>
                                    </Button>
                                </Grid>
                            </Tooltip>
                            }
                            <Tooltip title="Favorito" aria-label="add">
                                <Grid item>
                                    <Button style={{color: '#787777'}}>
                                        {isFavourite ?
                                            <BookmarkIcon fontSize="small"
                                                          style={{marginRight: '5px', color: '#d10019'}}
                                                          onClick={() => {
                                                              addBookmark(false)
                                                          }}/>
                                            : <BookmarkBorderSharpIcon fontSize="small" style={{marginRight: '5px'}}
                                                                       onClick={() => {
                                                                           addBookmark(true)
                                                                       }}/>}

                                    </Button>
                                </Grid>
                            </Tooltip>
                        </Grid>
                    </CardContent>
                    }
                </Card>
            </Grid>
            <Divider/>
            <Comments data={data}/>
        </Grid>
    )
}

export default (Discussion);
