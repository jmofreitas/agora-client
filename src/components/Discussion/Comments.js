import React, {useRef, useState} from 'react';
import {Comment, Form, Header} from 'semantic-ui-react';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid/Grid";
import Hidden from "@material-ui/core/Hidden/Hidden";
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import TextField from "@material-ui/core/TextField/TextField";
import SendIcon from '@material-ui/icons/Send';
import IconButton from "@material-ui/core/IconButton/IconButton";
import {useHistory, useParams} from "react-router-dom";
import makeStyles from "@material-ui/core/styles/makeStyles";
import useGlobalState from "../Context/useGlobalState";
import {addCommentMutation, getTopicById} from "../../queries/queries";
import {useMutation} from "@apollo/react-hooks";
import ChildComments from "./ChildComments";
import TokenDecode from "../../utilComponents/TokenDecode";
import DateParser from "../../utilComponents/DateParser";
import ReplyIcon from '@material-ui/icons/Reply';
import Collapse from "@material-ui/core/Collapse/Collapse";
import Typography from "@material-ui/core/Typography/Typography";
import InputBase from "@material-ui/core/InputBase";

const styles = {
    slide: {
        padding: 1,
        minHeight: 100,
        color: '#fff',
    },
};

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 3),
    },

    paper: {
        maxWidth: 400,
        padding: theme.spacing(1),
        boxShadow: 'none',
        display: "flex"
    },

    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },

    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },

    margin: {
        margin: theme.spacing(1),

    },

    cover: {
        width: '100%',
        maxHeight: '70%',
        borderRadius: '6px',
    },

    card: {
        boxShadow: 'none',
        border: 'none',
    },

    comment: {
        paddingBottom: '1rem',
    },
}));

function Comments(props) {

    const classes = useStyles();
    const history = useHistory();
    const globalState = useGlobalState();
    const currentReply = globalState.reply;
    const textInput = useRef(null);
    const {id} = useParams();
    const decoded = TokenDecode();

    const [addComment, {dataAdded}] = useMutation(addCommentMutation);
    const [comment, setComment] = useState('');
    const [expandedReply, setExpandedReply] = useState();

    const {data} = props;

    const changeReply = (_id, firstName, lastName) => {
        const newReply = {
            _id,
            firstName,
            lastName
        };

        globalState.setReply(newReply);
    };

    const submitReply = () => {
        addComment({
            variables: {
                parent: currentReply._id,
                topic: id,
                body: comment,
                user: decoded._id
            },
            refetchQueries: [
                {
                    query: getTopicById,
                    variables: {
                        id: id
                    }
                }
            ]
        });
        setComment('');
    };

    const handleExpandClickReply = (key) => {
            setExpandedReply(key);
    };

    return (
        <div style={{marginBottom: '30px'}}>
            <Comment.Group>
                <Header as='h3'>
                    Comentários
                </Header>
                {data.getTopicById.comments.map((data, key) => (
                    <div key={key} style={styles.slide}>
                        <Comment>
                            <Comment.Content>
                                <div style={{display: 'flex'}}>
                                    <Comment.Author>{data.user[0].firstName} {data.user[0].lastName}</Comment.Author>
                                    <Comment.Metadata>
                                        <div>{DateParser(data.created_at)}</div>
                                    </Comment.Metadata>
                                </div>
                                <Comment.Text>{data.body}</Comment.Text>
                                {/*<Hidden smDown>*/}
                                <Comment.Actions>
                                        <Comment.Action>
                                            <Button size="small" style={{color: '#787777'}}>
                                                <ThumbUpAltOutlinedIcon fontSize="small" style={{paddingRight: '5px'}}/>{data.upvotes}
                                            </Button>
                                        </Comment.Action>
                                        <Comment.Action>
                                            <Button style={{color: '#787777'}} onClick={() => {
                                                textInput.current.focus();
                                                changeReply(data._id, data.user[0].firstName, data.user[0].lastName)
                                            }}>
                                                <ReplyIcon fontSize="small"/>
                                            </Button>
                                        </Comment.Action>
                                </Comment.Actions>
                                {(data.children.length > 0 && (expandedReply !== key)) && <Comment.Actions>
                                    <Comment.Action style={{marginBottom: '10px'}}>
                                        <Typography className="redTitles" variant="caption"
                                                    onClick={() => handleExpandClickReply(key)}>ver {data.children.length} respostas</Typography>
                                    </Comment.Action>
                                </Comment.Actions>
                                }
                                {/*</Hidden>*/}
                                {/*<Comment.Actions>*/}
                                {/*<Comment.Action onClick={() => {*/}
                                {/*textInput.current.focus();*/}
                                {/*changeReply(data._id, data.user[0].firstName, data.user[0].lastName)*/}
                                {/*}}>Reply</Comment.Action>*/}
                                {/*</Comment.Actions>*/}
                            </Comment.Content>
                            {data.children.length > 0 &&
                            <Collapse in={expandedReply === key} timeout="auto" unmountOnExit>
                                <ChildComments child={data.children} comment={comment}/>
                            </Collapse>}
                        </Comment>
                    </div>
                ))}
                <Grid container style={{marginTop: '20px'}}>
                    <Grid item xs={11}>
                        <TextField
                            inputRef={textInput}
                            fullWidth
                            id="entry_message"
                            name="entry_message"
                            multiline
                            required
                            variant="outlined"
                            value={comment}
                            onChange={(e) => {
                                setComment(e.target.value)
                            }}
                            onKeyPress={(e) => {
                                if (e.key === 'Enter') {
                                    e.preventDefault();
                                    submitReply()
                                }
                            }}
                        />
                    </Grid>
                    <Grid item xs={1}>
                        <IconButton variant="contained" onClick={() => {
                            submitReply()
                        }}><SendIcon/></IconButton>
                    </Grid>
                </Grid>
            </Comment.Group>
        </div>
    )
}

export default Comments;