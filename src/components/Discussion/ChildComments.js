import React, {useState} from 'react';
import Comment from "semantic-ui-react/dist/commonjs/views/Comment/Comment";
import Button from "@material-ui/core/Button/Button";
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import ReplyIcon from '@material-ui/icons/Reply';
import Typography from "@material-ui/core/Typography/Typography";
import DateParser from "../../utilComponents/DateParser";
import Collapse from "@material-ui/core/Collapse/Collapse";
import useGlobalState from "../Context/useGlobalState";


function ChildComments(props) {

    const [expandedReply, setExpandedReply] = useState();
    const globalState = useGlobalState();

    const changeReply = (_id, firstName, lastName) => {
        const newReply = {
            _id,
            firstName,
            lastName
        };

        globalState.setReply(newReply);
    };

    const handleExpandClickReply = (key) => {
        setExpandedReply(key);
    };

    return (
        <div>
            <Comment.Group>
                {props.child.map((children, key) => (
                    <Comment key={key}>
                        <Comment.Content>
                            <div id={key} style={{display: 'flex'}}>
                                <Comment.Author>{children.user[0].firstName} {children.user[0].lastName}</Comment.Author>
                                <Comment.Metadata>
                                    <div>{DateParser(children.created_at)}</div>
                                </Comment.Metadata>
                            </div>
                            <Comment.Text>{children.body}</Comment.Text>
                            <Comment.Actions style={{marginTop: '10px'}}>
                                <Comment.Action>
                                    <Button style={{color: '#787777'}}>
                                        <ThumbUpAltOutlinedIcon fontSize="small"
                                                                style={{paddingRight: '5px'}}/>{children.upvotes}
                                    </Button>
                                </Comment.Action>
                                <Comment.Action>
                                    <Button style={{color: '#787777'}} onClick={() => {
                                        // textInput.current.focus();
                                        changeReply(children._id)
                                    }}>
                                        <ReplyIcon fontSize="small"/>
                                    </Button>
                                </Comment.Action>
                            </Comment.Actions>
                            {(children.children.length > 0 && (expandedReply !== key)) && <Comment.Actions>
                                <Comment.Action>
                                    <Typography className="redTitles"
                                                variant="caption"
                                                onClick={() => handleExpandClickReply(key)}>ver {children.children.length} respostas</Typography>
                                </Comment.Action>
                            </Comment.Actions>
                            }
                        </Comment.Content>
                        <Comment.Group>
                            {children.children.length > 0 &&
                            children.children.map((child, key) => (
                                <Collapse key={key} in={expandedReply === key} timeout="auto" unmountOnExit>
                                    {/*<ChildComments child={data.children} comment={comment}/>*/}
                                    <Comment>
                                        <Comment.Content>
                                            <div id={key} style={{display: 'flex'}}>
                                                <Comment.Author>{child.user[0].firstName} {child.user[0].lastName}</Comment.Author>
                                                <Comment.Metadata>
                                                    <div>{DateParser(child.created_at)}</div>
                                                </Comment.Metadata>
                                            </div>
                                            <Comment.Text>{child.body}</Comment.Text>
                                            <Comment.Actions style={{marginTop: '10px'}}>
                                                <Comment.Action>
                                                    <Button style={{color: '#787777'}}>
                                                        <ThumbUpAltOutlinedIcon fontSize="small"
                                                                                style={{paddingRight: '5px'}}/>{child.upvotes}
                                                    </Button>
                                                </Comment.Action>
                                                <Comment.Action>
                                                    <Button style={{color: '#787777'}} onClick={() => {
                                                        // textInput.current.focus();
                                                        changeReply(child._id)
                                                    }}>
                                                        <ReplyIcon fontSize="small"/>
                                                    </Button>
                                                </Comment.Action>
                                            </Comment.Actions>

                                            {child.children.length > 0 &&
                                            <Comment.Actions>
                                                <Comment.Action>
                                                    <Typography className="redTitles"
                                                                variant="caption"
                                                                onClick={() => handleExpandClickReply(key)}>
                                                        ver {children.children.length} respostas
                                                    </Typography>
                                                </Comment.Action>
                                            </Comment.Actions>
                                            }
                                        </Comment.Content>
                                    </Comment>
                                </Collapse>
                            ))}
                        </Comment.Group>
                    </Comment>
                ))}
            </Comment.Group>
        </div>
    )
}

export default ChildComments