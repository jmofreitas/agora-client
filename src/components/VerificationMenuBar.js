import React, {useEffect, useState} from 'react';
import {useLocation, useHistory} from 'react-router-dom';
import SecondaryMenuBar from "./SecondaryMenuBar";
import BottomMenu from "./BottomMenu";
import Hidden from "@material-ui/core/Hidden/Hidden";
import {useCookies} from "react-cookie";
import {useQuery} from "@apollo/react-hooks";
import {getUserById} from "../queries/queries";

const axios = require('axios');

function VerificationMenuBar(props) {

    const tokenIsUndefined = () => {
        return cookies.token === "undefined" || !cookies.token;
    };


    const [cookies, setCookie, removeCookie] = useCookies(['token', 'preferences']);
    const [style, setStyle] = useState('block');
    const [verified, setVerified] = useState(true);

    const history = useHistory();
    const location = useLocation();
    const [preferencesDone, setPreferencesDone] = useState(false);



    const verify = () => {
        axios.post('http://agora.jmfreitas.com/verifyToken', {
            data: {
                token: cookies.token
            }
        }).then(() => {return null}).catch((err) => {  removeCookie("token"); history.push('/login')})
    };

    const {loading, error, data} = useQuery(getUserById, {
        variables: {
            id: props.token._id
        }
    });

    const verifyData = () => {
        if (data.getUserById.preferences.length > 0) {
            setCookie('preferences', true);
        } else {
            setCookie('preferences', false);
            history.push('/');
        }
    };

    useEffect(() => {
        if (data)
            verifyData();
    }, [data]);

    useEffect(() =>{
            if(!tokenIsUndefined()){
                verify();
            }
    }, [cookies.token]);

    history.listen((location, action) => {
        if ((location.pathname === '/register') || (location.pathname === '/login')) {
            setStyle('none')
        }
        else {
            setStyle('block')
        }
    });

    useEffect(() => {
        if ((location.pathname === '/register') || (location.pathname === '/login')) {
            setStyle('none')
        }
        else {
            setStyle('block')
        }
        if (location.pathname === '/register') {
            setVerified(false);
        } else if (location.pathname === '/login') {
            setVerified(false);
        } else if (location.pathname === '/preferences') {
            setVerified(false);
        } else {
            setVerified(true)
        }
    }, [location.pathname]);

    return (
        <div>
            {(verified && !tokenIsUndefined() && cookies.preferences === 'true') &&
            <SecondaryMenuBar style={style}/>
            }

            {cookies.preferences === 'true' &&
            <Hidden mdUp>
                <BottomMenu style={style}/>
            </Hidden>
            }
        </div>
    )

}

export default VerificationMenuBar;