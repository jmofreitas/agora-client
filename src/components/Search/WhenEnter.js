import React, {useEffect, useState} from 'react';
import {fade, makeStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip/Chip";
import Typography from "@material-ui/core/Typography/Typography";
import ListItemAvatar from "@material-ui/core/ListItemAvatar/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar/Avatar";
import ListItemText from "@material-ui/core/ListItemText/ListItemText";
import ListItem from "@material-ui/core/ListItem/ListItem";
import {useHistory} from 'react-router-dom';
import {Card, CardContent} from "@material-ui/core";
import CardActions from "@material-ui/core/CardActions/CardActions";
import AvatarGroup from "@material-ui/lab/AvatarGroup/AvatarGroup";
import imgsTestes from "../../imgs/profile.jpg";
import SearchTopics from "./SearchTopics";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        // paddingLeft: '1rem',
        overflowY: 'hidden',
        // '& > *': {
        //     margin: theme.spacing(0.5),
        // }
    },

    tags: {
        // paddingLeft: '2rem',
        overflowY: 'hidden',
        '& > *': {
            margin: theme.spacing(0.5),
        }
    },

    friends: {
        '& > *': {
            margin: theme.spacing(2),
        },
        paddingTop: 10,
        width: '100%',
    },
    inline: {
        display: 'inline',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade("#E1E4E6", 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 10,
        marginTop: '1rem',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(6),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        paddingLeft: '3rem',
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    paper: {
        padding: theme.spacing(1),
        marginTop: '0.5rem',
        marginBottom: '0.5rem',
        marginLeft: '2rem',
        height: '1.8rem',
        verticalAlign: 'middle',
        fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
        paddingTop: '1rem',
        fontSize: '17px',
        backgroundColor: '#F4f4f4',
        boxShadow: 'none',
    },
    title: {
        fontSize: '22px',
        fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
        paddingLeft: '2rem',
    },

    discuss: {
        padding: theme.spacing(0.5),
        margin: 'auto',
        width: '100%',
        marginBottom: '1rem',
    },

    grid: {
        flexWrap: 'noWrap',
        margin: 'auto',
        padding: '1rem',
    },
    onHover: {
        '&hover': {
            cursor: 'pointer'
        }
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    },
}));

function WhenEnter(props) {
    const [value, setValue] = useState(0);

    const handleChange = (event, value) => {
        setValue(value)
    };

    const classes = useStyles();
    const history = useHistory();

    const [content, setContent] = useState([]);


    return (
        <div className="mainPadding">
            {(props.show === 0 || props.show === 1) &&
            <div style={{padding:10}}>
                {(props.categories.length > 0 || props.tags.length > 0) &&
                <Grid item xs={12}>
                    <Typography variant="h6">Tags</Typography>
                </Grid>
                }
                <div className={classes.tags}>
                    {props.categories.map((category, key) => (
                        <Chip className={`category ${classes.onHover}`} label={category.name} onClick={() => {
                        }}/>
                    ))}
                    {props.tags.map((tag, key) => {
                        if (props.categories.length + key + 1 < 10) {
                            return (
                                <Chip className={`tags ${classes.onHover}`} label={`#${tag.name}`} onClick={() => {
                                }}/>
                            )
                        }
                    })
                    }
                </div>
            </div>
            }
            {(props.show === 0 || props.show === 2) &&
            <div style={{padding:10}}>
                {(props.topics.length > 0) &&
                <Grid item xs={12}>
                    <Typography variant="h6">Discussões</Typography>
                </Grid>
                }
                <Grid container justify='center'>
                    <Grid item xs={12}>
                        {props.topics.map((topic, key) => (
                            <SearchTopics topic={topic} key={key} onClick={() => {
                                history.push(`/discussion/${topic._id}`);
                            }}/>
                        ))}
                    </Grid>
                </Grid>
            </div>
            }
            {(props.show === 0 || props.show === 3) &&
            <div style={{padding:10}}>
                {props.communities.length > 0 &&
                <Grid item xs={12}>
                    <Typography variant="h6">Comunidades</Typography>
                </Grid>
                }
                <Grid container className={classes.root}>
                    {props.communities.map((community, key) =>
                        (
                            <Card key={key} className="feedCommunityCard" style={{
                                background: `url(${community.image})`,
                                marginRight: "5px",
                                backgroundSize: "125%",
                                backgroundPosition: "center"
                            }} onClick={() => {
                                history.push(`/community/${community._id}`);
                            }}>
                                <CardContent className="feedCommunityBottomRectangle">
                                    <CardActions className="feedCommunityCardActions">
                                        <Grid container style={{justifyContent: 'center'}}>
                                            <Grid item>
                                                <Typography noWrap className="redTitlesNoPad communityTitle">
                                                    {community.name}
                                                </Typography>
                                            </Grid>
                                            <Grid item>
                                                <Typography noWrap className="communityMembers">
                                                    {community.number_members} Membros
                                                </Typography></Grid>
                                        </Grid>
                                    </CardActions>
                                </CardContent>

                            </Card>
                        )
                    )}
                </Grid>
            </div>
            }
            {(props.show === 0 || props.show === 4) &&
            <div style={{paddingTop: '1rem'}}>
                {props.users.length > 0 &&
                <Grid item xs={12}>
                    <Typography variant="h6">Utilizadores</Typography>
                </Grid>
                }
                {props.users.map((user, key) => (
                    <ListItem onClick={() => {
                        history.push(`/profile/${user._id}`);
                    }}>
                        <ListItemAvatar>
                            <Avatar alt={`${user.firstName} ${user.lastName}`} src={user.image}/>
                        </ListItemAvatar>
                        <ListItemText
                            primary={`${user.firstName} ${user.lastName}`}
                        />
                    </ListItem>
                ))}
            </div>
            }
        </div>
    )
}

export default WhenEnter;
