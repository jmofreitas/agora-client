import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {

        padding: '1rem',
    },
    paper: {
        padding: theme.spacing(0.5),
        margin: 'auto',
        width: '100%',
        marginBottom: '1rem',
    },

    grid: {
        flexWrap: 'noWrap',
        margin: 'auto',
        padding: '1rem',
    }

}));


function SearchTopics(props) {

    const history = useHistory();
    const classes = useStyles();
    return (
        <Grid container className="p10Top" onClick={() => {
            history.push(`/discussion/${props.topic._id}`);
        }}>
            <Grid container spacing={1}>
                <Paper className={classes.paper}>
                    <Grid container spacing={1} className={classes.grid}>
                        <Grid item xs={12} sm container>
                            <Grid item xs container direction="column" spacing={2}>
                                <Grid item xs>
                                    <Typography gutterBottom variant="caption" color="secondary">
                                        {props.topic.category[0].name}
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        {props.topic.title}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary">
                                        {props.topic.upvotes} gostos
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    )
}

export default SearchTopics;