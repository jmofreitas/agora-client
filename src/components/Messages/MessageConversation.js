import React, {useEffect, useRef, useState} from 'react';
import Row from '@material-ui/core/TableRow';
import {useParams} from 'react-router-dom';
import {
    ThemeProvider,
    Message,
    MessageList,
    MessageGroup,
    MessageText,
    TextComposer,
    SendButton,
} from '@livechat/ui-kit';
import {useMutation, useQuery} from "@apollo/react-hooks";
import {
    addMessageMutation,
    getConversationById,
    getMessagesByConversation,
    getUserById
} from "../../queries/queries";
import TokenDecode from "../../utilComponents/TokenDecode";
import LoadingComponent from "../styleComponents/LoadingComponent";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import DateParser from "../../utilComponents/DateParser";
import useGlobalState from "../Context/useGlobalState";


function MessageConversation() {
    const {id} = useParams();
    const messageInput = useRef(null);
    const ownId = TokenDecode()._id;
    const [toSkip, setToSkip] = useState(0);
    const [user2, setUser2] = useState('');
    const [heightMessage, setHeightMessage] = useState('auto');
    const [messageContent, setMessageContent] = useState('');
    const [sendDisabled, setSendDisabled] = useState(true);
    const [toShow, setToShow] = useState([]);
    const [addMessage, {data: dataMessage}] = useMutation(addMessageMutation);

    const {loading: loadingConversation, error: errorConversation, data: dataConversation} = useQuery(getConversationById, {
        variables: {
            _id: id
        }
    });

    const {loading: loadingUser, error: errorUser, data: dataUser} = useQuery(getUserById, {
        variables: {id: ownId},
    });


    const {loading: loadingMessages, error: errorMessages, data: dataMessages, refetch: refetchMessages} = useQuery(getMessagesByConversation, {
        variables: {
            conversation: id,
            skip: toSkip
        }
    });

    const handleSubmit = () => {
        addMessage({
            variables: {
                from: ownId,
                to: user2,
                body: messageContent,
                conversation: id
            },
            refetchQueries: [
                {
                    query: getMessagesByConversation, variables: {
                        conversation: id,
                        skip: toSkip
                    }
                }
            ]
        }).then(() => {
            setMessageContent("");
        })
    };

    const onButtonClick = () => {
        // `current` points to the mounted text input element
        setHeightMessage(60);
    };


    const scrollComponent = () => {
        let left = 0;
        window.scrollTo({
            top: document.body.scrollHeight,
            left: left,
            behavior: 'smooth'
        });
    };

    const groupWrapper = (object, image) => {
        const wrap = [];
        wrap.push(<MessageGroup
            onlyFirstWithMeta>{object}</MessageGroup>);
        return wrap;
    };

    useEffect(() => {
        if (messageContent === '') {
            setSendDisabled(true)
        } else {
            setSendDisabled(false)
        }
    }, [messageContent]);

    useEffect(() => {
        if (dataConversation && dataConversation.getConversationById) {
            console.log(dataConversation.getConversationById);
            if (dataConversation.getConversationById[0].user_1[0]._id !== ownId) {
                setUser2(dataConversation.getConversationById[0].user_1[0]._id)
            } else {
                setUser2(dataConversation.getConversationById[0].user_2[0]._id)
            }
        }
    }, [dataConversation]);

    useEffect(() => {
        const timer = setInterval(() => {
            refetchMessages()
        }, 10000);
        return () => {
            clearInterval(timer)
        }
    }, []);


    useEffect(() => {
        let arrayPush = [];
        if (dataUser && dataConversation && dataMessages && user2 !== '') {
            console.log(dataMessages);
            if (dataMessages.getMessagesByConversation.length > 0) {
                let checkDifference = dataMessages.getMessagesByConversation[0].from;
                let semiPush = [];
                dataMessages.getMessagesByConversation.map((message, key) => {
                    if (message.from === ownId) {
                        if (key === dataMessages.getMessagesByConversation.length - 1) {
                            console.log('ultimo ', message.body);
                            semiPush.push(<Message date={DateParser(message.timestamp)} isOwn={true}>
                                <MessageText style={{
                                    border: 'solid',
                                    borderRadius: '10px',
                                    borderWidth: 'thin',
                                    borderColor: 'lightgrey'
                                }}>
                                    {message.body}
                                </MessageText>
                            </Message>);
                            arrayPush.push(groupWrapper(semiPush, 'no'));
                            semiPush = [];

                        } else if (checkDifference !== message.from) {
                            checkDifference = message.from;
                            arrayPush.push(groupWrapper(semiPush, 'no'));
                            semiPush = [];
                        }
                        semiPush.push(<Message date={DateParser(message.timestamp)} isOwn={true}>
                            <MessageText style={{
                                border: 'solid',
                                borderRadius: '10px',
                                borderWidth: 'thin',
                                borderColor: 'lightgrey'
                            }}>
                                {message.body}
                            </MessageText>
                        </Message>);
                    } else {
                        if (key === dataMessages.getMessagesByConversation.length - 1) {
                            console.log('ultimo ', message.body);
                            semiPush.push(<Message date={DateParser(message.timestamp)}>
                                <MessageText style={{backgroundColor: 'lightgray', borderRadius: '10px'}}>
                                    {message.body}
                                </MessageText>
                            </Message>);
                            arrayPush.push(groupWrapper(semiPush, 'no'));
                            semiPush = [];
                        } else if (checkDifference !== message.from) {
                            checkDifference = message.from;
                            arrayPush.push(groupWrapper(semiPush, 'no'));
                            semiPush = [];
                        }
                        semiPush.push(<Message date={DateParser(message.timestamp)}>
                            <MessageText style={{backgroundColor: 'lightgray', borderRadius: '10px'}}>
                                {message.body}
                            </MessageText>
                        </Message>);
                    }
                });
                setToShow(arrayPush);
            }
        }
    }, [dataUser, dataConversation, dataMessages, user2]);

    useEffect(() => {
        scrollComponent();
    }, []);

    if (loadingConversation || loadingMessages || loadingUser) return <LoadingComponent/>;

    if (errorConversation || errorMessages || errorUser) return <p>erro</p>;


    return (
        <ThemeProvider>
            <Grid container justify='flex-start'>
                <Grid item xs={12} md={8}
                      style={{marginBottom: heightMessage !== 'auto' ? heightMessage - 20 : 40}}>
                    <MessageList style={{maxWidth: '100%', padding: '1.5em'}}>
                        <div>
                            {toShow}
                        </div>
                    </MessageList>
                </Grid>
                <Grid container justify='flex-start'
                      style={{bottom: 60, position: 'fixed', maxWidth: '100%'}}>
                    <Grid xs={12} md={8} item>
                        <TextComposer ref={messageInput} onClick={() => {
                            onButtonClick()
                        }}>
                            <Row style={{display: 'flex'}}>
                                <TextField
                                    value={messageContent}
                                    id="standard-multiline-static"
                                    multiline

                                    defaultValue="Default Value"
                                    variant='outlined'
                                    onChange={(e) => {
                                        setMessageContent(e.target.value);
                                    }}

                                    onKeyPress={(ev) => {
                                        if (ev.key === 'Enter') {
                                            ev.preventDefault();
                                            handleSubmit()
                                        }
                                    }}

                                    style={{border: 'none', height: 'auto', width: '100%'}}/>
                                <SendButton disabled={sendDisabled} style={{marginLeft: '2vh', marginRight:'1vh'}} onClick={() => {
                                    handleSubmit()
                                }}/>
                            </Row>
                            <Row verticalAlign="bottom" justify="right">
                            </Row>
                        </TextComposer>
                    </Grid>
                </Grid>
            </Grid>
        </ThemeProvider>
    )
}

export default MessageConversation;
