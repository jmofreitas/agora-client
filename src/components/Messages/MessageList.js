import React, {useEffect, useRef, useState} from 'react';
import {fade, makeStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import {useHistory} from 'react-router-dom';
import imgsTestes from "../../imgs/profile.jpg";
import dog from "../../imgs/transferir.jpg";
import Grid from "@material-ui/core/Grid";
import {Paper, Typography} from "@material-ui/core";

import useOutsideClick from "../Context/useOutsideClick";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {addConversationMutation, getConversationsByUser, getTopics, getUserByName} from "../../queries/queries";
import TokenDecode from "../../utilComponents/TokenDecode";
import LoadingComponent from "../styleComponents/LoadingComponent";
import TextField from "@material-ui/core/TextField";
import useGlobalState from "../Context/useGlobalState";

const useStyles = makeStyles(theme => ({
    root: {
        paddingLeft: 10,

    },

    friends: {
        display: 'flex',
        '& > *': {
            margin: theme.spacing(2),
        },
        paddingTop: 10,
        width: '100%',
    },

    inline: {
        display: 'inline',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade("#E1E4E6", 0.25),
        },

        marginRight: theme.spacing(2),
        marginLeft: 10,
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(6),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    medium: {
        width: theme.spacing(5),
        height: theme.spacing(5),
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    userCard: {
        '&:hover': {
            cursor: 'pointer',
            backgroundColor: fade("#E1E4E6", 0.25),
        },
    }
}));

function MessageList() {
    const classes = useStyles();
    const history = useHistory();

    const ref = useRef();

    const id = TokenDecode()._id;

    const [showUsers, setShowUsers] = useState(false);
    const [searchValue, setSearchValue] = useState('');
    const [user_1, setUser_1] = useState(id);
    const [user_2, setUser_2] = useState('');
    const [searchingText, setSearchingText] = useState('');
    const [addConversation, {data: dataConversation}] = useMutation(addConversationMutation);
    const globalState = useGlobalState();

    const changeMessage = (firstName, lastName) => {
        const newMessage = {
            firstName,
            lastName
        };

        globalState.setMessage(newMessage);
    };
    const {loading: loadingUser, error: errorUser, data: dataUser, refetch: refetchUser} = useQuery(getUserByName,
        {
            variables: {
                name: searchValue
            }
        });

    const {loading: loadingAllConversation, error: errorAllConversations, data: dataAllConversations, refetch: refetchConversation} = useQuery(getConversationsByUser,
        {
            variables: {
                user_1: id
            }
        });


    useOutsideClick(ref, () => {
        setShowUsers(false);
    });

    useEffect(() => {
        if (searchValue !== '') {
            refetchUser().then(() => {
                setShowUsers(true);
            })
        } else {
            setShowUsers(false)
        }
    }, [searchValue]);

    useEffect(() => {
        refetchConversation()
    }, []);


    const goToConversation = (user_2) => {
        addConversation({
            variables: {
                user_1,
                user_2
            }
        })
    };

    useEffect(() => {
        if (dataConversation)
            history.push(`/messages/${dataConversation.addConversation._id}`)
    }, [dataConversation]);

    if (loadingAllConversation || loadingUser) return <LoadingComponent/>;

    return (
        <div>
            <div className={classes.search}>
                <div className={classes.searchIcon}>
                    <SearchIcon/>
                </div>
                <InputBase
                    placeholder="Search…"
                    value={searchingText}
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                    }}
                    onChange={(e) => {
                        setSearchingText(e.target.value)
                    }}

                    onKeyPress={(ev) => {
                        if (ev.key === 'Enter') {
                            ev.preventDefault();
                            setSearchValue(ev.target.value)
                        }
                    }}
                    inputProps={{'aria-label': 'search'}}
                />
            </div>
            {(showUsers && dataUser) ?
                <div className={classes.search} ref={ref}>
                    <Grid container style={{position: 'absolute', zIndex: 9999}}>
                        {dataUser.getUserByName.map((user, key) => (
                            <Grid key={key} item xs={12} onClick={() => {
                                changeMessage(user.firstName, user.lastName);
                                goToConversation(user._id)
                            }}>
                                <Paper className={classes.userCard} elevation={1} variant='outlined' square
                                       style={{padding: 10}}>
                                    <Grid item xs={12} container alignContent='center'>
                                        <Grid item xs={3}>
                                            <Avatar alt="Cindy Baker" src={user.image} className={classes.medium}
                                                    onClick={() => {
                                                        history.push("/messages/details");
                                                    }}/>
                                        </Grid>
                                        <Grid item xs={5} alignContent='center' alignItems='center' container>
                                            {`${user.firstName} ${user.lastName}`}
                                        </Grid>
                                        <Grid item xs={4} alignContent='center' container>
                                            <Typography
                                                variant='caption'>
                                                Utilizador Normal
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Paper>
                            </Grid>
                        ))}
                    </Grid>
                </div>
                :
                <div>

                    {(dataAllConversations && dataAllConversations.getConversationsByUser.length > 0) ? <List className={classes.root}>
                        {dataAllConversations.getConversationsByUser.map((conversation, key) => (
                            <ListItem alignItems="flex-start" onClick={() => {
                                changeMessage(conversation.user_1[0]._id !== id ? conversation.user_1[0].firstName
                                    : conversation.user_2[0].firstName, conversation.user_1[0]._id !== id ? conversation.user_1[0].lastName
                                    : conversation.user_2[0].lastName);
                                history.push(`/messages/${conversation._id}`);
                            }}>
                                <ListItemAvatar>
                                    <Avatar alt="Remy Sharp"
                                            src={conversation.user_1[0]._id !== id ? conversation.user_1[0].image
                                                : conversation.user_2[0].image}/>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={conversation.user_1[0]._id !== id ? conversation.user_1[0].firstName + ' ' + conversation.user_1[0].lastName
                                        : conversation.user_2[0].firstName + ' ' + conversation.user_2[0].lastName}
                                    secondary={
                                        <React.Fragment>

                                        </React.Fragment>
                                    }
                                />
                            </ListItem>
                        ))}

                    </List>

                        : <Typography style={{display: 'flex', paddingTop: '16rem', justifyContent: 'center', alignItems: 'center'}} variant="body2" color="textSecondary"> Não tem mensagens </Typography>

                    }

                </div>}
        </div>
    );
}

export default (MessageList);
