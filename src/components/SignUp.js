import React, {useEffect, useRef, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import publico from '../imgs/publico.png';
import {useHistory} from 'react-router-dom';
import {useCookies} from "react-cookie";
import FormHelperText from "@material-ui/core/FormHelperText";

const axios = require('axios');

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        width: theme.spacing(7),
        height: theme.spacing(7)
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const ColorButton = withStyles(theme => ({
    root: {
        color: theme.palette.getContrastText('#D10019'),
        backgroundColor: '#D10019',
        '&:hover': {
            backgroundColor: '#D10019',
        },
    },
}))(Button);

function SignUp() {
    const classes = useStyles();
    const history = useHistory();
    const [firstName, setFirstName] = useState('');
    const [error, setError] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [url, setUrl] = useState("");
    const fileInput = useRef(null);
    const [cookies] = useCookies(['cookie-name']);
    const [btnDisabled, setBtnDisabled] = useState(true);

    useEffect(() => {
        if (cookies.token) {
            if (cookies.token !== "undefined") {
                redirect();
            }
        }
    }, []);

    const redirect = () => {
        return history.push('/login');

    };

    const savePost = () => {
        axios({
            method: 'post',
            url: 'http://agora.jmfreitas.com/user',
            auth: {
                username: email,
                password
            },
            data: {
                firstName,
                lastName,
                image: url
            }
        }).then(() => {
            history.push("/login");
        }).catch(error => {
            console.log(error.response.data);
            setError(error.response.data);
        });
    };

    useEffect(() => {
        if (firstName && lastName && email && password && url) {
            setBtnDisabled(false)
        }
    }, [firstName, lastName, email, password, url]);

    function onSubmit(e) {
        e.preventDefault();
        const data = new FormData();
        const imagefile = document.querySelector('#imgFile');
        data.append("type", "user");
        data.append("imgFile", imagefile.files[0]);
        axios.post('http://agora.jmfreitas.com/upload', data, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            setUrl(response.data.url);
        });
    }

    function fileNames() {
        const { current } = fileInput;

        if (current && current.files.length > 0) {
            let messages = [];
            for (let file of current.files) {
                messages = messages.concat(<p key={file.name}>{file.name}</p>);
            }
            return messages;
        }
        return null;
    }


    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar variant="square" src={publico} className={classes.avatar}>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Registo
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            autoComplete="fname"
                            name="firstName"
                            variant="outlined"
                            value={firstName}
                            fullWidth
                            id="primeiroNome"
                            label="Primeiro Nome"
                            onChange={event => {
                                setFirstName(event.target.value)
                            }}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            variant="outlined"
                            value={lastName}
                            fullWidth
                            id="lastName"
                            label="Último Nome"
                            name="ultimoNome"
                            onChange={event => {
                                setLastName(event.target.value)
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            value={email}
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            onChange={event => {
                                setEmail(event.target.value)
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            value={password}
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            onChange={event => {
                                setPassword(event.target.value)
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <input
                            id="imgFile"
                            type="file"
                            ref={fileInput}
                            // The onChange should trigger updates whenever
                            // the value changes?
                            // Try to select a file, then try selecting another one.
                            onChange={onSubmit}
                        />
                        {fileNames()}
                    </Grid>
                </Grid>
                {error && <FormHelperText style={{marginTop: 20, fontSize: 15}} error id="component-error-text">{error}</FormHelperText>}
                <ColorButton
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={(e) => {
                        e.preventDefault();
                        savePost()
                    }}
                    disabled={btnDisabled}
                >
                    Registar
                </ColorButton>
                <Grid container justify="flex-end">
                    <Grid item>
                        <Link href="/login" variant="body2">
                            Já possui uma conta? Faça login
                        </Link>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
}


export default SignUp