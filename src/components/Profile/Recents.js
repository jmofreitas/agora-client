import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import ImgDiscuss from "../../imgs/transferir.jpg";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        paddingLeft: 10,
        paddingRight: 10,


    },
    paper: {
        maxWidth: 400,
        margin: `${theme.spacing(0)}px auto`,
        padding: 7,
        boxShadow: 'none',

    },

    timeFontSize: {
        fontSize: 12,
    },

}));


function Recents() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container wrap="nowrap" spacing={2}>
                    <Grid item>
                        <Avatar/>
                    </Grid>
                    <Grid item xs>
                        <Typography>Comentou uma publicação: "Concordo completamente"</Typography>
                        <Typography className={classes.timeFontSize}>há x horas</Typography>
                    </Grid>
                </Grid>
                <hr/>
            </Paper>
            <Paper className={classes.paper}>
                <Grid container wrap="nowrap" spacing={2}>
                    <Grid item>
                        <Avatar/>
                    </Grid>
                    <Grid item xs>
                        <Typography>Aderiu à comunidade: "Los Salpicos"</Typography>
                        <Typography className={classes.timeFontSize}>há x dias</Typography>
                    </Grid>
                </Grid>
                <hr/>
            </Paper>
            <Paper className={classes.paper}>
                <Grid container wrap="nowrap" spacing={2}>
                    <Grid item>
                        <Avatar/>
                    </Grid>
                    <Grid item xs>
                        <Typography>Gostou do comentário: "Concordo com o seu ponto de vista!"</Typography>
                        <Typography className={classes.timeFontSize}>há x dias</Typography>
                    </Grid>
                </Grid>
                <hr/>
            </Paper>
            <Paper className={classes.paper}>
                <Grid container wrap="nowrap" spacing={2}>
                    <Grid item>
                        <Avatar/>
                    </Grid>
                    <Grid item xs>
                        <Typography>Criou o tópico de discussão: "CR7 vs Messi"</Typography>
                        <Typography className={classes.timeFontSize}>há x dias</Typography>
                    </Grid>
                </Grid>
                <hr/>
            </Paper>

            <Paper className={classes.paper}>
                <Grid container wrap="nowrap" spacing={2}>
                    <Grid item>
                        <Avatar/>
                    </Grid>
                    <Grid item xs>
                        <Typography>Comentou uma publicação: "Não jogam nada pá..."</Typography>
                        <Typography className={classes.timeFontSize}>há x dias</Typography>
                    </Grid>
                </Grid>
                <hr/>
            </Paper>

            <Paper className={classes.paper}>
                <Grid container wrap="nowrap" spacing={2}>
                    <Grid item>
                        <Avatar/>
                    </Grid>
                    <Grid item xs>
                        <Typography>Aderiu à comunidade: "FCP Fans"</Typography>
                        <Typography className={classes.timeFontSize}>há x dias</Typography>
                    </Grid>
                </Grid>
            </Paper>



        </div>
    );
}

export default Recents;