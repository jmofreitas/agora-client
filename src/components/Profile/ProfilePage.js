import React, {useEffect, useState} from 'react';
import ImgProfile from "../../imgs/mlemtest.jpg";
import {makeStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import ChatOutlinedIcon from '@material-ui/icons/ChatOutlined';
import ExitToAppRoundedIcon from '@material-ui/icons/ExitToAppRounded';
import Recents from "./Recents";
import ProfileCommunities from "./ProfileCommunities";
import ProfileFavourites from "./ProfileFavourites";
import {getUserById} from "../../queries/queries";
import TokenDecode from "../../utilComponents/TokenDecode";
import {useQuery} from "@apollo/react-hooks";
import LoadingComponent from "../styleComponents/LoadingComponent";
import Avatar from "@material-ui/core/Avatar/Avatar";
import {useParams} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        boxShadow: 'none',
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 600,
        boxShadow: 'none',
        paddingLeft: 10,
    },
    icons: {
        paddingLeft: 10,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    paper2: {
        maxWidth: 50,
        boxShadow: 'none',
        fontSize: 15,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
        borderRadius: '50%',
    },
    large: {
        width: theme.spacing(10),
        height: theme.spacing(10),
    },
}));

function ProfilePage() {
    const classes = useStyles();
    const {id} = useParams();
    const [value, setValue] = useState(0);
    const [content, setContent] = useState(0);
    const [upvotes, setUpvotes] = useState(0);
    const [receivedBadges, setReceivedBadges] = useState([]);

    const {loading: loadingUser, error: errorUser, data: dataUser, refetch: refetchUser} = useQuery(getUserById, {
        variables: {
            id: id
        },

    });

    const handleChange = (event, value) => {
        setValue(value)
    };

    const displayContent = () => {
        if (loadingUser) return <LoadingComponent/>;
        setContent(dataUser.getUserById);
        setUpvotes(Number(dataUser.getUserById.karma.comments) + Number(dataUser.getUserById.karma.topics));
        setReceivedBadges(dataUser.getUserById.receivedBadges);
    };

    useEffect(() => {
        refetchUser()
    }, []);

    useEffect(() => {
        displayContent()
    }, [loadingUser]);

    if (loadingUser) return <LoadingComponent/>;
    console.log(dataUser);
    return (
        <div className={classes.root}>
            {/*<MenuBarProfile/>*/}
            {/*<SecondaryMenuBar/>*/}
            <Paper className={classes.paper}>
                <Grid container justify='center'>
                    <Grid item style={{alignItems: 'center', flexWrap: 'nowrap'}} container spacing={2} xs={10}>
                        <Grid item>
                            {dataUser.getUserById.image &&
                            <Avatar src={dataUser.getUserById.image} className={classes.large}/>}

                        </Grid>
                        <Grid className={classes.grid} item xs={12} sm container>
                            <Grid item xs container direction="column" spacing={2}>
                                <Grid item xs>
                                    <Typography gutterBottom variant="h6" className="menuTitle" style={{
                                        width: "100%",
                                        whiteSpace: "nowrap",
                                        overflow: "hidden",
                                        textOverflow: "ellipsis"
                                    }}>
                                        {dataUser.getUserById.firstName} {dataUser.getUserById.lastName}
                                    </Typography>
                                </Grid>

                                <Grid className={classes.icons} container spacing={0}>
                                    <Grid item xs>
                                        <ThumbUpAltOutlinedIcon style={{paddingRight: '5px'}} className="profileIcon"/>
                                        {dataUser.getUserById.karma.comments + dataUser.getUserById.karma.topics}
                                    </Grid>
                                    <Grid item xs>
                                        <ChatOutlinedIcon style={{paddingRight: '5px'}} className="profileIcon"/>
                                        {dataUser.getUserById.nrComments}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={1}/>
                </Grid>
            </Paper>
            <Grid container>
                <Paper className={classes.root}>
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="secondary"
                        textColor="secondary"
                        centered
                        /* variant="fullWidth"     */
                    >
                        <Tab label="Atividade Recente"/>
                        <Tab label="Comunidades"/>
                        <Tab label="Favoritos"/>
                    </Tabs>
                    {value === 0 && <Recents/>}
                    {value === 1 && <ProfileCommunities/>}
                    {value === 2 && <ProfileFavourites/>}

                </Paper>
            </Grid>
        </div>
    );
}

export default ProfilePage;


