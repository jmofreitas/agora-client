import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Card, CardContent} from "@material-ui/core";
import CardActions from "@material-ui/core/CardActions/CardActions";
import AvatarGroup from "@material-ui/lab/AvatarGroup/AvatarGroup";
import Avatar from "@material-ui/core/Avatar/Avatar";
import imgsTestes from "../../imgs/transferir.jpg";
import {useHistory} from 'react-router-dom';
import {useQuery} from "@apollo/react-hooks";
import TokenDecode from "../../utilComponents/TokenDecode";
import LoadingComponent from "../styleComponents/LoadingComponent";
import {getCommunitiesByUser} from "../../queries/queries";


const useStyles = makeStyles(theme => {
    return ({
        root: {
            display: 'flex',
            overflowY: 'hidden',
            '& > *': {
                margin: theme.spacing(1),
            },
        },
        space: {
            marginTop: '1.5rem'
        },
        small: {
            width: theme.spacing(3),
            height: theme.spacing(3),
        }
    });
});

function ProfileCommunities() {
    const classes = useStyles();
    const history = useHistory();
    const decoded = TokenDecode();

    const {loading: loading, error, data} = useQuery(getCommunitiesByUser, {
        variables: {
            user: decoded._id
        }
    });

    if (loading) return <LoadingComponent/>;

    return (
        <div>
            {(data.getCommunitiesByUser && data.getCommunitiesByUser.length > 0) ?
                data.getCommunitiesByUser[0].community.map((community, key) => (
                    <Grid container justify="center" key={key} className={classes.root}>
                        <Card className="feedCommunityCard" style={{background: `url(${community.image})`}}
                              onClick={() => {
                                  history.push(`/community/${community._id}`);
                              }}>
                            <CardContent className="feedCommunityBottomRectangle">
                                <CardActions className="feedCommunityCardActions">
                                    <Grid container style={{justifyContent: 'center'}}>
                                        <Grid item>
                                            <Typography noWrap className="redTitlesNoPad communityTitle">
                                                {community.name}
                                            </Typography>
                                        </Grid>
                                        <Grid item>
                                            <Typography noWrap className="communityMembers">
                                                {community.number_members} membros
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </CardActions>
                            </CardContent>
                        </Card>
                    </Grid>
                )) :
                <Grid container justify='center' style={{marginTop: 20}}>
                    <Grid item xs={12} container justify='center'>
                        <Typography>Não tem comunidades</Typography>
                    </Grid>
                </Grid>
            }
        </div>
    );
}

export default ProfileCommunities;
