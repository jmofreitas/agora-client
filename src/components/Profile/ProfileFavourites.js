import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {useQuery} from "@apollo/react-hooks";
import {getFavouritesByUser} from "../../queries/queries";
import TokenDecode from "../../utilComponents/TokenDecode";
import LoadingComponent from "../styleComponents/LoadingComponent";
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(0.5),
        margin: 'auto',
        maxWidth: 500,
        marginBottom: '1rem',
    },

    grid: {
        flexWrap: 'noWrap',
        margin: 'auto',
        padding: '1rem',
    }

}));

function ProfileFavourites() {
    const classes = useStyles();
    const history = useHistory();
    const {loading: loadingFavourites, error: errorFavourites, data: dataFavourites} = useQuery(getFavouritesByUser,
        {
            variables: {
                user: TokenDecode()._id
            }
        });

    if (loadingFavourites) return <LoadingComponent/>;

    return (
        <div className="mainPadding">
            {(dataFavourites && dataFavourites.getFavouritesByUser.length > 0) ? dataFavourites.getFavouritesByUser.map((favourite, key) => (
                    <Paper className={classes.paper} onClick={() => {
                        history.push(`/discussion/${favourite.topic[0]._id}`);
                    }}>
                        <Grid container spacing={1} className={classes.grid}>
                            <Grid item xs={12} sm container>
                                <Grid item xs container direction="column" spacing={2}>
                                    <Grid item xs>
                                        <Typography gutterBottom variant="caption" color="secondary">
                                            {favourite.topic[0].category[0].name}
                                        </Typography>
                                        <Typography variant="body1" gutterBottom>
                                            {favourite.topic[0].title}
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary">
                                            {favourite.topic[0].upvotes} gostos
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>
                )
                ) :
                <Grid container justify='center' style={{marginTop: 20}}>
                    <Grid item xs={12} container justify='center'>
                        <Typography>Não tem favoritos</Typography>
                    </Grid>
                </Grid>
            }
        </div>
    );
}

export default ProfileFavourites;