import Grid from "@material-ui/core/Grid/Grid";
import Hidden from "@material-ui/core/Hidden/Hidden";
import SwipeableViews from "react-swipeable-views";
import Card from "@material-ui/core/Card/Card";

import Typography from "@material-ui/core/Typography/Typography";

import Button from "@material-ui/core/Button/Button";
import ChatOutlinedIcon from "@material-ui/icons/ChatOutlined";
import React from "react";
import Rating from '@material-ui/lab/Rating';


import imgsBruno from "../../imgs/1.jpg";
import imgsFilme2 from "../../imgs/2.jpg";
import imgsFilme3 from "../../imgs/3.jpg";
import imgsFilme4 from "../../imgs/4.jpg";
import imgsFilme5 from "../../imgs/5.jpg";
import imgsFilme6 from "../../imgs/6.jpg";
import Paper from "@material-ui/core/Paper/Paper";
import FeedCommunity from "../home/FeedCommunity";


const styles = {
    root: {
        paddingRight: '1rem',
        paddingLeft: '1rem',
        overflowY: 'hidden',

    },
    feedCommunity: {
        paddingRight: '40px',
        paddingLeft: '20px',
    },
    slideContainer: {
        paddingRight: '5px',
        width: 125,

    },

    desktopSlideContainer: {
        paddingRight: '5px',
        width: 255,


    },

    feedCommunitySlideContainer: {
        paddingRight: '100px',
    },
    slide: {
        minHeight: 200,
        color: '#fff',

        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        width: 125,
    },
    desktopCardContainer: {
        paddingTop: 10,

    },

    paper: {
        padding: 0.5,
        margin: 'auto',
        width: '100%',
        marginBottom: '1rem',

    },

    grid: {
        flexWrap: 'noWrap',
        margin: 'auto',
        padding: '1rem',
    },

    cover: {
        width: '60%',
        borderRadius: '4px',
    },

    card: {
        boxShadow: 'none',
        border: 'none',
    },

    paperDesktop: {
        padding: 0.5,
        margin: 'auto',
        width: '50%',
        marginBottom: '1rem',

    },

};


function CinemaPage() {

    const [value, setValue] = React.useState(2);

    return (

        <Grid container className="mainPadding">
            <Typography variant="h6" style={{margin: '0.5rem'}}>Destaques</Typography>
            <Hidden mdUp>

            <Grid container>
                    <SwipeableViews style={styles.root} slideStyle={styles.slideContainer}>
                        <Card style={{
                            minHeight: 200,
                            color: '#fff',
                            backgroundImage: `url(${imgsBruno})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 125,
                        }}
                            // onClick={() => {history.push("/discussion");}}

                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-10rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                        <Card style={{
                            minHeight: 200,
                            color: '#fff',
                            backgroundImage: `url(${imgsFilme2})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 125,
                        }}
                            // onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-10rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                        <Card style={{
                            minHeight: 200,
                            color: '#fff',
                            backgroundImage: `url(${imgsFilme3})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 125,
                        }}
                            // onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-10rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                        <Card style={{
                            minHeight: 200,
                            color: '#fff',
                            backgroundImage: `url(${imgsFilme4})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 125,
                        }}
                            // onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-10rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                        <Card style={{
                            minHeight: 200,
                            color: '#fff',
                            backgroundImage: `url(${imgsFilme5})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 125,
                        }}
                            // onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-10rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                        <Card style={{
                            minHeight: 200,
                            color: '#fff',
                            backgroundImage: `url(${imgsFilme6})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 125,
                        }}
                            // onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-10rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>


                    </SwipeableViews>
            </Grid>
        </Hidden>


    <Hidden smDown>
        <Grid container>
            <Grid container style={{ overflowY: "hidden"}}>

                    <div style={{paddingRight: '40px',paddingLeft: '20px', display: "flex"}}>
                        <Card style={{
                            backgroundImage: `url(${imgsBruno})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >

                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>

                        </Card>


                        <Card style={{
                            backgroundImage: `url(${imgsFilme2})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >

                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>

                        </Card>

                        <Card style={{
                            backgroundImage: `url(${imgsFilme3})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >

                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>

                        </Card>

                        <Card style={{
                            backgroundImage: `url(${imgsFilme4})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >

                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>

                        </Card>

                        <Card style={{
                            backgroundImage: `url(${imgsFilme5})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >

                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>

                        </Card>

                        <Card style={{
                            backgroundImage: `url(${imgsFilme6})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                        <Card style={{
                            backgroundImage: `url(${imgsFilme6})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                        <Card style={{
                            backgroundImage: `url(${imgsFilme6})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center center',
                            width: 225,
                            color: '#fff',
                            marginLeft: 10,
                            minHeight: 325,
                        }}
                            //   onClick={() => {history.push("/discussion");}}
                        >
                            <div>
                                <Button size="small" color="primary" style={{bottom: '-18rem'}}>
                                    <ChatOutlinedIcon/>200
                                </Button>
                            </div>
                        </Card>

                    </div>
            </Grid>
            </Grid>
            </Hidden>

            <Grid container>
                <Grid item xs={12} className="feedItem">
                    <Typography variant="h6" style={{margin: '0.5rem'}}>Dos nossos críticos</Typography>
                </Grid>

                <Paper style={styles.paper}>
                    <Grid container spacing={1} style={styles.grid}>
                        <Grid item xs={12} sm container>
                            <Grid item xs container direction="column" spacing={2}>
                                <Grid item xs>
                                    <div style={{display: 'flex'}}>
                                        <Typography style={{paddingRight: '0.5rem'}} gutterBottom variant="caption"
                                                    color="secondary">
                                            O Filme do Bruno Aleixo
                                        </Typography>
                                        <Rating size="small" name="read-only" value={value} readOnly/>
                                    </div>

                                    <Typography variant="body1" gutterBottom>
                                        Jorge Mourinho
                                    </Typography>

                                    <Typography noWrap variant="body2" color="textSecondary">
                                        As comédias portuguesas costumam cair em clichés e tipos de humor muito arcaicos
                                        e repetitivos.
                                        O Filme do Bruno Aleixo evita ser mais um desses filmes que, segundo Busto
                                        durante a conversa,
                                        “só têm atores a fazer caretas parvas no cartaz”.
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>

                <Paper style={styles.paper}>
                    <Grid container spacing={1} style={styles.grid}>
                        <Grid item xs={12} sm container>
                            <Grid item xs container direction="column" spacing={2}>
                                <Grid item xs>
                                    <div style={{display: 'flex'}}>
                                        <Typography style={{paddingRight: '0.5rem'}} gutterBottom variant="caption"
                                                    color="secondary">
                                            O Filme do Bruno Aleixo
                                        </Typography>
                                        <Rating size="small" name="read-only" value={value} readOnly/>
                                    </div>

                                    <Typography variant="body1" gutterBottom>
                                        Jorge Mourinho
                                    </Typography>

                                    <Typography noWrap variant="body2" color="textSecondary">
                                        As comédias portuguesas costumam cair em clichés e tipos de humor muito arcaicos
                                        e repetitivos.
                                        O Filme do Bruno Aleixo evita ser mais um desses filmes que, segundo Busto
                                        durante a conversa,
                                        “só têm atores a fazer caretas parvas no cartaz”.
                                    </Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Paper>

            </Grid>


            <Grid container>


                <Grid item xs={12} className="feedItem">
                    <Typography variant="h6" style={{margin: '1rem'}}>Em breve</Typography>
                </Grid>

                <Hidden mdUp>
                    <Card style={{
                        backgroundImage: `url(${imgsFilme5})`,
                        backgroundSize: 'cover',
                        backgroundPosition: 'center top',
                        width: 360,
                        color: '#fff',
                        margin: 'auto',
                        minHeight: 225,
                    }}>
                    </Card>

                    <Typography variant="title" style={{marginTop: '1rem', width: '100%', textAlign: 'center'}}>Once Upon a Time in
                        Hollywood</Typography>
                    <Typography variant="caption" color="textSecondary"
                                style={{ marginBottom: '1rem', width: '100%', textAlign: 'center'}}>Estreia a 14/08/2019</Typography>

                    <Paper style={styles.paper}>
                        <Grid container spacing={1} style={styles.grid}>
                            <Grid item xs={12} sm container>
                                <Grid item xs container direction="column" spacing={2}>
                                    <Grid item xs>
                                        <Typography style={{paddingRight: '0.5rem', margin: 'auto'}} gutterBottom
                                                    variant="body1"
                                                    color="secondary">
                                            O Filme do Bruno Aleixo
                                        </Typography>
                                        <Typography style={{paddingRight: '0.5rem', margin: 'auto'}} variant="caption"
                                                    color="textSecondary" gutterBottom>
                                            Estreia a 23/01/2019
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>

                </Hidden>
                <Grid container style={{display: 'block', textAlign: 'center'}}>
                    <Hidden smDown>
                        <Card style={{
                            backgroundImage: `url(${imgsFilme5})`,
                            backgroundSize: 'cover',
                            backgroundPosition: 'center top',
                            width: 540,
                            color: '#fff',
                            margin: 'auto',
                            minHeight: 525,
                        }}>
                        </Card>
                        <Grid style={{margin: '1rem'}}>
                            <Grid item>
                                <Typography variant="title">Once Upon a Time in
                                    Hollywood
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="caption" color="textSecondary"
                                >
                                    Estreia a 14/08/2019
                                </Typography>
                            </Grid>
                        </Grid>

                        <Paper style={styles.paperDesktop}>
                            <Grid container spacing={1} style={styles.grid}>
                                <Grid item xs={12} sm container>
                                    <Grid item xs container direction="column" spacing={2}>
                                        <Grid item xs>
                                            <Typography style={{paddingRight: '0.5rem', margin: 'auto'}} gutterBottom
                                                        variant="body1"
                                                        color="secondary">
                                                O Filme do Bruno Aleixo
                                            </Typography>
                                            <Typography style={{paddingRight: '0.5rem', margin: 'auto'}}
                                                        variant="caption"
                                                        color="textSecondary" gutterBottom>
                                                Estreia a 23/01/2019
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Paper>

                    </Hidden>
                </Grid>

                <Grid item xs={12} className="feedItem">
                    <Typography variant="h6" style={{margin: '0.5rem'}}>Dos nossos leitores</Typography>
                </Grid>

                <Grid container>
                    <Paper style={styles.paper}>
                        <Grid container spacing={1} style={styles.grid}>
                            <Grid item xs={12} sm container>
                                <Grid item xs container direction="column" spacing={2}>
                                    <Grid item xs>
                                        <div style={{display: 'flex'}}>
                                            <Typography style={{paddingRight: '0.5rem'}} gutterBottom variant="caption"
                                                        color="secondary">
                                                O Filme do Bruno Aleixo
                                            </Typography>

                                        </div>

                                        <Typography variant="body1" gutterBottom>
                                            Joana Silva
                                        </Typography>

                                        <Typography noWrap variant="body2" color="textSecondary">
                                            As comédias portuguesas costumam cair em clichés e tipos de humor muito
                                            arcaicos
                                            e repetitivos.
                                            O Filme do Bruno Aleixo evita ser mais um desses filmes que, segundo Busto
                                            durante a conversa,
                                            “só têm atores a fazer caretas parvas no cartaz”.
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>

                    <Paper style={styles.paper}>
                        <Grid container spacing={1} style={styles.grid}>
                            <Grid item xs={12} sm container>
                                <Grid item xs container direction="column" spacing={2}>
                                    <Grid item xs>
                                        <div style={{display: 'flex'}}>
                                            <Typography style={{paddingRight: '0.5rem'}} gutterBottom variant="caption"
                                                        color="secondary">
                                                O Filme do Bruno Aleixo
                                            </Typography>

                                        </div>

                                        <Typography variant="body1" gutterBottom>
                                            Joana Silva
                                        </Typography>

                                        <Typography noWrap variant="body2" color="textSecondary">
                                            As comédias portuguesas costumam cair em clichés e tipos de humor muito
                                            arcaicos
                                            e repetitivos.
                                            O Filme do Bruno Aleixo evita ser mais um desses filmes que, segundo Busto
                                            durante a conversa,
                                            “só têm atores a fazer caretas parvas no cartaz”.
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12} className="feedItem">
                    <Typography variant="h6" style={{margin: '0.5rem'}}>Comunidades</Typography>
                </Grid>
                <FeedCommunity/>
            </Grid>

        </Grid>


    )
}

export default CinemaPage;