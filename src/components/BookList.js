import React from 'react';
import {useQuery} from '@apollo/react-hooks'
import {getBooksQuery} from "../queries/queries";

function BookList() {
    const {loading, error, data} = useQuery(getBooksQuery);

    const displayBooks = () => {
        if (loading) return <p>Loading ...</p>;
        return data.books.map(book => {
            return (
                <li key={book.id}>{book.name}</li>
            )
        })
    };

    return(
        <div>
            <ul id='book-list'>
                {displayBooks()}
            </ul>
        </div>
    )

}

export default BookList;
