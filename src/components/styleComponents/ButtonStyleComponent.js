import React from 'react';
import {styled} from '@material-ui/core/styles';
import {makeStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import SearchIcon from '@material-ui/icons/Search';

import Button from '@material-ui/core/Button';
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction/BottomNavigationAction";

const useStyles = makeStyles({
    root: {
        // background:'DarkSalmon',
        color: props =>  props.color === 'red' ? 'red' : 'blue',
    },
});

function MyButton(props) {
    const {color, ...other} = props;
    const classes = useStyles(props);
    return <Button className={classes.root} {...other} />;
}

MyButton.propTypes = {
    color: PropTypes.oneOf(['blue', 'red']).isRequired,
};


export default function ButtonStyleComponent(props) {
    console.log("icon >", props.icon);
    let Icon = props.icon;
    return (
        <React.Fragment>
            <MyButton color={props.color}><Icon/></MyButton>
        </React.Fragment>
    )
}