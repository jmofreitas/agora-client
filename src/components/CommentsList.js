import React from 'react';
import {useQuery} from '@apollo/react-hooks'
import {getTopic} from "../queries/queries";
import AddComment from "../queries/testQueries/AddComment";
import LoadingComponent from "./styleComponents/LoadingComponent";

function CommentsList() {
    const {loading, error, data} = useQuery(getTopic, {
        variables: { id: '5de43f906ab19228d535c255' },
    });

    const displayTopicComments = () => {
        if (loading) return <LoadingComponent/>;
        return data ? data.topic.comments.map((value) => {
            return <div>
                        <li>{value.body}</li>
                    </div>
        }) : [];
    };

    return(
        <div>
            <AddComment/>
            <ul>
                {displayTopicComments()}
            </ul>
        </div>
    )

}

export default CommentsList;
