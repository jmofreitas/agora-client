import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import imgsTestes from "../../imgs/profile.jpg";

const useStyles = makeStyles(theme => ({
    root: {
        paddingTop: '0px',
    },

    inline: {
        display: 'inline',
    },

    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },

    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
}));

function Notifications() {
    const classes = useStyles();

    return (
            <List className={classes.root}>
                <ListItem style={{ alignItems: 'center', maxWidth: '100%', padding: '0.9em', backgroundColor: 'lightGray', borderBottomColor: 'lightGray',
                    borderBottomWidth: '0.1px',
                    borderBottomStyle: 'solid' }}>
                    <ListItemAvatar>
                        <Avatar alt="Remy Sharp" src={imgsTestes}/>
                    </ListItemAvatar>
                    <Typography noWrap>Joana Silva gostou do teu comentário na discussão "Rui Pinto: criminoso ou justiceiro?"</Typography>
                </ListItem>
                <ListItem style={{ alignItems: 'center', maxWidth: '100%', padding: '0.9em', borderBottomColor: 'lightGray',
                    borderBottomWidth: '0.1px',
                    borderBottomStyle: 'solid'}}>
                    <ListItemAvatar>
                        <Avatar alt="Remy Sharp" src={imgsTestes}/>
                    </ListItemAvatar>
                    <Typography noWrap>Tiago Lopes marcou a tua discussão "Ver o ambiente por quem não tem compromissos partidários" como favotita</Typography>
                </ListItem>
                <ListItem style={{ alignItems: 'center', maxWidth: '100%', padding: '0.9em', borderBottomColor: 'lightGray',
                    borderBottomWidth: '0.1px',
                    borderBottomStyle: 'solid' }}>
                    <ListItemAvatar>
                        <Avatar alt="Remy Sharp" src={imgsTestes}/>
                    </ListItemAvatar>
                    <Typography noWrap>Tatiana Azevedo gostou do teu comentário na discussão "Democracia Digital, Cidadania Participada"</Typography>
                </ListItem>
            </List>
    );
}

export default (Notifications);