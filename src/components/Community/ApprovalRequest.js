import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import dog from "../../imgs/transferir.jpg";
import CheckIcon from '@material-ui/icons/Check';
import Fab from "@material-ui/core/Fab";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper/Paper";
import ClearIcon from '@material-ui/icons/Clear';
import {useHistory} from 'react-router-dom';
import {
    getPendingOnCommunity,
    getTopics,
    updatePendingUserOnCommunity,
    updateTopicMutation
} from "../../queries/queries";
import LoadingComponent from "../styleComponents/LoadingComponent";
import {useMutation, useQuery} from "@apollo/react-hooks";
import {useParams} from 'react-router-dom';
import AvatarGroup from "@material-ui/lab/AvatarGroup/AvatarGroup";

const useStyles = makeStyles(theme => ({
    root: {
        padding: '2rem',
    },
    paper: {
        padding: '1rem',
        margin: 'auto',
        textAlign: 'center',
        width: "100%",
        marginTop: 20
    },
    grid: {
        flexWrap: 'noWrap',
        margin: 'auto',
        padding: '1rem',
        display: 'block',
        alignItems: 'center',
        width: '100%'
    },
    margin: {
        margin: theme.spacing(1),
    },
}));

function ApprovalRequests() {
    const classes = useStyles();
    const history = useHistory();
    const [content, setContent] = useState([]);
    const {id} = useParams();
    const [updateUser] = useMutation(updatePendingUserOnCommunity);

    const {loading: loading, error, data} = useQuery(getPendingOnCommunity, {
        variables: {
            community: id
        }
    });

    const displayContent = () => {
        if (loading) return <LoadingComponent/>;
        setContent(data.getPendingOnCommunity);
    };

    const acceptRequest = (model) => {
        updateUser(
            {
                variables: {
                    _id: model._id,
                    status: true
                },
                refetchQueries:[
                    {
                        query: getPendingOnCommunity,
                            variables: {
                                community: id
                            }
                    }
                ]
            })
    };

    const rejectRequest = (model) => {
        updateUser(
            {
                variables: {
                    _id: model._id,
                    status: false
                },
                refetchQueries:[
                    {
                        query: getPendingOnCommunity,
                        variables: {
                            community: id
                        }
                    }
                ]
            })
    };

    useEffect(() => {
        displayContent();
    }, [loading]);

    useEffect(() => {
        if (data) {
            if (data.getPendingOnCommunity !== content) {
                setContent(data.getPendingOnCommunity);
            }
        }
    }, [data]);

    if (loading) return <LoadingComponent/>;

    return (
        <Grid container className="p10Top">
            <Grid container spacing={1}>
                {content.length > 0 ? content.map((request, key) =>
                    (
                <Paper className={classes.paper} key={key}>
                    <Grid container spacing={1}>
                        <Grid item xs className={classes.grid}>
                            <Typography style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                                <Typography style={{display: "flex"}}>
                                    <div style={{marginRight: 2}}>
                                        {request.user[0].image ? <Avatar alt="Remy Sharp" src={request.user[0].image} onClick={() => {history.push(`/profile/${request.user[0]._id}`);}}/>
                                        : <Avatar alt="Remy Sharp" src={dog} onClick={() => {history.push(`/profile/${request.user[0]._id}`);}}/>}
                                    </div>
                                    <Typography variant="body1" className={'textNameRequests'} style={{paddingTop: 7.5}}>
                                        {request.user[0].firstName} {request.user[0].lastName}
                                    </Typography>
                                </Typography>
                            </Typography>
                            <Typography variant="body1" className={'textNameRequests'}>
                                {request.entry_message}
                            </Typography>
                            <Fab size="small" style={{background: "#228B22", color: "white"}} className={classes.margin} onClick={(e) => {e.preventDefault(); acceptRequest(request)}}>
                                <CheckIcon/>
                            </Fab>
                            <Fab size="small" style={{background: "#D10019", color: "white"}} className={classes.margin} onClick={(e) => {e.preventDefault(); rejectRequest(request)}}>
                                <ClearIcon/>
                            </Fab>
                        </Grid>
                    </Grid>
                </Paper>
                    )
                ): <Typography variant="body1">Não possui pedidos pendentes</Typography>}
            </Grid>
        </Grid>
    );
}

export default ApprovalRequests;
