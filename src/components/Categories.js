import React, {useEffect, useState} from "react";
import {useParams} from 'react-router-dom';
import CinemaPage from "./CinemaPage/CinemaPage";
import {useCookies} from "react-cookie";
import {useQuery} from "@apollo/react-hooks";
import {getCategories} from "../queries/queries";
import LoadingComponent from "./styleComponents/LoadingComponent";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import {useHistory, useLocation} from 'react-router-dom';
import FeedOthers from "./home/FeedOthers";

function Categories() {

    const {id} = useParams();
    const [toWrite, setToWrite] = useState([]);
    const [value, setValue] = useState(0);
    const [writeTabs, setWriteTabs] = useState([]);
    const [categories, setCategories] = useState([]);
    const [cookies] = useCookies(['token', 'preferences']);

    const {loading, error, data} = useQuery(getCategories);

    const history = useHistory();
    const location = useLocation();

    const handleChange = (event, value) => {
        setValue(value)
    };

    useEffect(() => {
        if (!loading)
            setCategories(data.getCategories);
    }, [loading]);

    useEffect(() => {
        if (categories.length > 0)
            verifyValue();
    }, [categories]);

    const verifyValue = () => {
        if (error) return [];
        if (loading) return <LoadingComponent/>;

        let arrayPush = [];
        let verified = false;
        // categories.unshift({_id: 'feed1', name: 'Início', image: null});
        arrayPush.push(<Tab label="Início" onClick={() => {
            history.push(`/`)
        }}/>);

        categories.map((data, key) => {

            arrayPush.push(
                <Tab label={data.name} key={key} onClick={() => {
                    history.push(`/category/${data.name}`)
                }}/>
            );


            if (location.pathname === '/' && !verified) {
                verified = true;
                setValue(key+1);
            }
            else if (location.pathname.startsWith('/category/') && !verified) {
                let path = '/category/' + data.name;
                if (location.pathname === path) {
                    verified = true;
                    setValue(key+1);
                }
            }
            return null;
        });
        setWriteTabs(arrayPush);
    };

    useEffect(() => {
        if (categories) {
            switch(id){
                case 'Cinema':
                    setToWrite(<CinemaPage/>);
                    break;
                default:
                    setToWrite(<FeedOthers model={categories}/>);
                    break;
            }
        }
    },[id, categories]);

    return (
        <div>
            {(location.pathname.startsWith("/category/") || location.pathname === "/" && cookies.preferences === 'true') &&
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="secondary"
                variant="scrollable"
                scrollButtons="auto"
            >
                {writeTabs}
            </Tabs>
            }
            {toWrite}
        </div>
    )
}

export default Categories;