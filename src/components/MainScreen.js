import React, {useEffect, useState} from 'react';
import FirstSelect from "./home/FirstSelect";
import Feed from "./home/Feed";
import {useQuery} from '@apollo/react-hooks';
import {getUserById} from "../queries/queries";
import LoadingComponent from "./styleComponents/LoadingComponent";
import useCookies from "react-cookie/cjs/useCookies";

function MainScreen(props) {
    const [preferencesDone, setPreferencesDone] = useState(false);
    const [cookies] = useCookies(['preferences']);


    console.log(props);


    return (
        <div>
            {
                cookies.preferences === "false" ? <FirstSelect/> : <Feed/>
            }
        </div>
    );
}

export default MainScreen;