import React, {createContext, useContext, useReducer} from 'react';

// ACTIONS
const SET_REPLY = 'SET_REPLY';
const SET_MESSAGE_NAME = 'SET_MESSAGE_NAME';

// CONTEXT + REDUCER FOR UPDATE
const GlobalStateContext = createContext();

const initialState = {
    reply: {
        _id: null,
        firstName: null,
        lastName: null
    },
    message: {
        firstName: null,
        lastName: null,
    }
};

const globalStateReducer = (state, action) => {
    switch (action.type) {
        case SET_REPLY:
            return {
                ...state,
                reply: {...action.payload}
            };
        case SET_MESSAGE_NAME:
            return{
                ...state,
                message: {...action.payload}
            };
        default:
            return state
    }
};

export const GlobalStateProvider = ({children}) => {
    const [state, dispatch] = useReducer(globalStateReducer, initialState);

    return <GlobalStateContext.Provider value={[state, dispatch]}>
        {children}
    </GlobalStateContext.Provider>
};

const useGlobalState = () => {
    const [state, dispatch] = useContext(GlobalStateContext);

    const setReply = ({_id, firstName, lastName}) => {
        dispatch({
            type: SET_REPLY,
            payload: {
                _id,
                firstName,
                lastName
            }
        });
    };

    const setMessage = ({firstName, lastName}) => {
        dispatch({
            type: SET_MESSAGE_NAME,
            payload: {
                firstName,
                lastName,
            }
        })
    };

    return {
        setReply,
        setMessage,
        reply: {...state.reply},
        message: {...state.message}
    };
};

export default useGlobalState;



