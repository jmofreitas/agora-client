import React, {useEffect, useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import ChatIcon from '@material-ui/icons/Chat';
import {useHistory} from 'react-router-dom';
import {useQuery} from "@apollo/react-hooks";
import {getTopics, getTopicsRecOrCat} from "../../queries/queries";
import LoadingComponent from "../styleComponents/LoadingComponent";
import makeStyles from "@material-ui/core/styles/makeStyles";
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import TokenDecode from "../../utilComponents/TokenDecode";
import {useParams} from "react-router-dom";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
});

function FeedLatest(props) {
    const classes = useStyles();
    const history = useHistory();
    const [content, setContent] = useState([]);
    const {loading, error, data} = useQuery(getTopics);
    const [categoryToUse, setCategoryToUse] = useState('null');
    const {id} = useParams();
    const {loading: loadingCheck, error: errorCheck, data: dataCheck, refetch: refetchTopics} = useQuery(getTopicsRecOrCat,
        {
            variables: {
                userRequesting: TokenDecode()._id,
                category: categoryToUse
            }
        });

    const goToDiscussion = (id) => {
        history.push(`/discussion/${id}`)
    };

    const displayContent = () => {
            if (loadingCheck) return <LoadingComponent/>;
            setContent(dataCheck.getTopicsRecOrCat);

    };

    const makeRequest = () => {
        if(props.model)
        if ( props.model.length && id) {
            props.model.map((category) => {
                if (category.name === id) {
                    setCategoryToUse(category._id)
                }
            });
        }
    };

    useEffect(() => {
        displayContent();
    }, [loadingCheck]);

    useEffect(() => {
        makeRequest();
    }, [props.model, id]);

    if (loadingCheck) return <LoadingComponent/>;
    return (
        <div style={{overflowY: "hidden", display: "flex"}}>
            {(dataCheck && dataCheck.getTopicsRecOrCat) && dataCheck.getTopicsRecOrCat.map((topic, key) =>
                (
                    <Card style={{
                        background: `url(${topic.metaImage})`,
                        backgroundSize: 'cover',
                        backgroundPosition: 'center center',
                        overflow: "initial",
                        color: "white",
                        marginRight: "10px"
                    }} key={key} onClick={(e) => {
                        e.preventDefault();
                        goToDiscussion(topic._id)
                    }}>
                        <div style={{backgroundColor: 'rgba(0,0,0,0.4)', height: "100%", width: '200px'}}>
                            <CardContent>
                                <Typography variant="h6">{topic.category[0].name}</Typography>
                                <Typography style={{paddingTop: '1rem'}} noWrap>{topic.title}</Typography>
                            </CardContent>
                            <CardActions style={{paddingTop: '3rem'}}>
                                <Button size="small" color="primary">
                                    <ThumbUpAltOutlinedIcon fontSize="small" style={{marginRight: 5}}/> {topic.upvotes}
                                </Button>
                                <Button size="small" color="primary">
                                    <ChatIcon fontSize="small"
                                              style={{marginRight: 5}}/> {topic.nrComments ? topic.nrComments : 0}
                                </Button>
                            </CardActions>
                        </div>
                    </Card>
                )
            )}
        </div>
    )
}

export default FeedLatest;