import React, {useEffect, useState} from 'react';
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import FeedLatest from "./FeedLatest";
import FeedCinema from "./FeedCinema";
import FeedCommunity from "./FeedCommunity";
import FeedMayLike from "./FeedMayLike";
import FeedHighLights from "./FeedHighLights";
import Paper from "@material-ui/core/Paper/Paper";
import {makeStyles} from "@material-ui/core";
import CinemaPage from "../CinemaPage/CinemaPage";

const useStyles = makeStyles(theme => ({
    root: {

        padding: '1rem',
    },
    paper: {
        padding: theme.spacing(0.5),
        margin: 'auto',
        width: '100%',
        marginBottom: '1rem',
    },

    grid: {
        flexWrap: 'noWrap',
        margin: 'auto',
        padding: '1rem',
    }

}));

function FeedOthers(props) {
    const classes = useStyles();

    return (
        <div>
            <Grid container className="mainPadding">
                <FeedHighLights model={props.model} category={"Categoria"} topic={"Tópico"}/>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography variant="h6">Últimas</Typography>
                    </Grid>
                    <Grid container className="p10Top">
                        <FeedLatest model={props.model} category={"Categoria"} topic={"Tópico"}/>
                    </Grid>
                </Grid>
                <Grid container  style={{paddingTop: '10px', paddingBottom: '10px'}}>
                    <Grid item xs={12}>
                        <Typography variant="h6">Comunidades</Typography>
                    </Grid>
                    <FeedCommunity model={props.model} category={"Categoria"} topic={"Tópico"}/>
                </Grid>
                <Grid container>
                    <Grid container className="p10Top">
                        <Grid container spacing={1}>
                            <Paper className={classes.paper}>
                                <Grid container spacing={1} className={classes.grid}>
                                    <Grid item xs={12} sm container>
                                        <Grid item xs container direction="column" spacing={2}>
                                            <Grid item xs>
                                                <Typography gutterBottom variant="caption" color="secondary">
                                                    CATEGORIA
                                                </Typography>
                                                <Typography variant="body1" gutterBottom>
                                                    Artista com "fome" comeu banana de 120 mil dólares
                                                </Typography>
                                                <Typography variant="body2" color="textSecondary">
                                                    123 gostos
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )

}

export default FeedOthers;