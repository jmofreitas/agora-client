import React, {useEffect, useState} from 'react';
import {Card, CardContent, makeStyles} from "@material-ui/core";
import CardActions from "@material-ui/core/CardActions";
import Grid from "@material-ui/core/Grid";
import Typography from '@material-ui/core/Typography';
import {useHistory} from 'react-router-dom';
import AddIcon from '@material-ui/icons/Add';
import {useQuery} from "@apollo/react-hooks";
import {getCommunitiesRec} from "../../queries/queries";
import LoadingComponent from "../styleComponents/LoadingComponent";
import TokenDecode from "../../utilComponents/TokenDecode";
import {useParams} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        overflowY: 'hidden',
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    }

}));

function FeedCommunity(props) {
    const classes = useStyles();
    const history = useHistory();
    const [categoryToUse, setCategoryToUse] = useState('null');
    const {id} = useParams();
    const {loading: loadingCheck, error: errorCheck, data: dataCheck, refetch: refetchTopics} = useQuery(getCommunitiesRec,
        {
            variables: {
                userRequesting: TokenDecode()._id,
                category: categoryToUse
            }
        });

    const makeRequest = () => {
        if(props.model)
            if ( props.model.length && id) {
                props.model.map((category) => {
                    if (category.name === id) {
                        setCategoryToUse(category._id)
                    }
                });
            }
    };

    useEffect(() => {
        makeRequest();
    }, [props.model, id]);

    if (loadingCheck) return <LoadingComponent/>;

    return (
        <Grid className={classes.root}>
            <Card className="feedCreateCommunity" onClick={() => {
                history.push("/newcommunity");
            }}>
                <CardContent style={{padding: '5px'}}>
                    <AddIcon style={{fontSize: '50px'}}/>
                </CardContent>
            </Card>
            {dataCheck.getCommunitiesRec.map((community, key) =>
                (
                    <Card key={key} className="feedCommunityCard" style={{
                        background: `url(${community.image})`,
                        marginRight: "5px",
                        backgroundSize: "125%",
                        backgroundPosition: "center"
                    }} onClick={() => {
                        history.push(`/community/${community._id}`);
                    }}>
                        <CardContent className="feedCommunityBottomRectangle">
                            <CardActions className="feedCommunityCardActions">
                                <Grid container style={{justifyContent: 'center'}}>
                                    <Grid item>
                                        <Typography noWrap className="redTitlesNoPad communityTitle">
                                            {community.name}
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        <Typography noWrap className="communityMembers">
                                            {community.number_members} Membros
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </CardActions>

                        </CardContent>
                    </Card>
                )
            )}
        </Grid>
    )
}

export default FeedCommunity;