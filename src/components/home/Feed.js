import React, {useEffect, useState} from 'react';
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid';
import FeedLatest from "./FeedLatest";
import FeedCinema from "./FeedCinema";
import FeedCommunity from "./FeedCommunity";
import FeedMayLike from "./FeedMayLike";
import FeedHighLights from "./FeedHighLights";
import {useCookies} from 'react-cookie';
import {useQuery} from "@apollo/react-hooks";
import {getCategories} from "../../queries/queries";
import LoadingComponent from "../styleComponents/LoadingComponent";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import {useHistory, useLocation} from 'react-router-dom';

function Feed() {
    const [value, setValue] = useState(0);
    const [writeTabs, setWriteTabs] = useState([]);
    const [categories, setCategories] = useState([]);
    const [cookies] = useCookies(['token', 'preferences']);

    const {loading, error, data} = useQuery(getCategories);

    const history = useHistory();
    const location = useLocation();

    const handleChange = (event, value) => {
        setValue(value)
    };

    useEffect(() => {
        if (!loading)
            setCategories(data.getCategories);
    }, [loading]);

    useEffect(() => {
        if (categories.length > 0)
            verifyValue();
    }, [categories]);

    const verifyValue = () => {
        if (error) return [];
        if (loading) return <LoadingComponent/>;

        let arrayPush = [];
        let verified = false;
        // categories.unshift({_id: 'feed1', name: 'Início', image: null});
        arrayPush.push(<Tab label="Início" onClick={() => {
            history.push(`/`)
        }}/>);

        categories.map((data, key) => {

            arrayPush.push(
                <Tab label={data.name} key={key} onClick={() => {
                    history.push(`/category/${data.name}`)
                }}/>
            );


            // if (location.pathname === '/' && !verified) {
            //     verified = true;
            //     setValue(key);
            // }
            // else if (location.pathname.startsWith('/category/') && !verified) {
            //     let path = '/category/' + data.name;
            //     if (location.pathname === path) {
            //         verified = true;
            //         setValue(key);
            //     }
            // }
            return null;
        });
        setWriteTabs(arrayPush);
    };

    return (
        <div>
            {(location.pathname.startsWith("/category/") || location.pathname === "/" && cookies.preferences === 'true') &&
            <Tabs
                value={value}
                onChange={handleChange}
                indicatorColor="secondary"
                textColor="secondary"
                variant="scrollable"
                scrollButtons="auto"
            >
                {writeTabs}
            </Tabs>
            }
            <Grid container className="mainPadding">
                <FeedHighLights category={"Categoria"} topic={"Tópico"}/>
                <Grid container>
                    <Grid item xs={12} className="feedItem">
                        <Typography variant="h6">Últimas</Typography>
                    </Grid>
                    <FeedLatest model={categories} category={"Categoria"} topic={"Tópico"}/>
                    <FeedCinema/>
                </Grid>
                <Grid container>
                    <Grid item xs={12} className="feedItem">
                        <Typography variant="h6">Últimas</Typography>
                    </Grid>
                    <FeedLatest model={categories} category={"Categoria"} topic={"Tópico"}/>
                </Grid>
                <Grid container>
                    <Grid item xs={12} className="feedItem">
                        <Typography variant="h6">Comunidades</Typography>
                    </Grid>
                    <FeedCommunity category={"Categoria"} topic={"Tópico"}/>
                </Grid>
                <Grid container>
                    <Grid item xs={12} className="feedItem">
                        <Typography variant="h6">Poderá gostar de</Typography>
                    </Grid>
                    <FeedMayLike category={"Categoria"} topic={"Tópico"}/>
                </Grid>
                <Grid container>
                    <Grid item xs={12} className="feedItem">
                        <Typography variant="h6">Sociedade</Typography>
                    </Grid>
                    <FeedLatest model={categories} category={"Categoria"} topic={"Tópico"}/>
                </Grid>
            </Grid>
        </div>
    )

}

export default Feed;