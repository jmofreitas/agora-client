import React, {useEffect, useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import imgsTestes from '../../imgs/transferir.jpg';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {useMutation, useQuery} from "@apollo/react-hooks";
import {addPreferenceMutation, getCategories} from "../../queries/queries";
import LoadingComponent from "../styleComponents/LoadingComponent";
import TokenDecode from "../../utilComponents/TokenDecode";
import useCookies from "react-cookie/cjs/useCookies";
import {useHistory} from "react-router-dom";

function FirstSelect() {

    const [selectedCat, setSelectedCat] = useState([]);
    const [categories, setCategories] = useState([]);
    const {loading, error, data} = useQuery(getCategories);
    const [preferences, {data: dataPref}] = useMutation(addPreferenceMutation);

    const[cookies, setCookie] = useCookies(['preferences']);

    const decoded = TokenDecode();

    function toggleCat(id) {
        setSelectedCat(prevState => {
            if (prevState.includes(id)) {
                return prevState.filter(cat => cat !== id)
            }

            return [...prevState, id]
        })
    }

    const handleSubmit = () => {
        selectedCat.map((cat) => {
            preferences({
                variables: {
                    category: cat,
                    user: decoded._id
                }
            })
        });
        setCookie('preferences', true);
    };

    useEffect(() => {
        if(dataPref){
            window.location.reload();
        }
    }, [dataPref]);

    useEffect(() => {
        if (data) {
            if (data.getCategories.length > 0) {
                // data.getCategories.splice(0, 1);
                setCategories(data.getCategories);
            }
        }
    }, [data]);

    if (error) return [];
    if (loading) return <LoadingComponent/>;

    return (
        <div id="welcomeCategories">
            {(categories.length > 0 && cookies.preferences === 'false') &&
            <Grid className="mainPadding" container>
                <Grid item xs={12}>
                    <Typography variant="subtitle1" style={{fontWeight: 'bold'}}>Em que está interessado?</Typography>
                </Grid>
                <Grid item xs={12} style={{marginBottom: 10, marginTop: 10}}>
                    <Typography variant="subtitle1">Utilizaremos as suas preferências para mostrar conteúdo
                        personalizado</Typography>
                </Grid>
                <Grid container spacing={3} className="mainPadding">
                    {categories.map((category, key) => (
                        <Grid item xs={6} md={2} id={key}>
                            <Card className="welcomeCategories" style={
                                category.image === null ? {backgroundColor: 'gray'} : {
                                    backgroundImage: 'url(' + category.image + ')',
                                    backgroundSize: 'cover',
                                    backgroundRepeat: 'no-repeat'
                                }}
                                  onClick={() => toggleCat(category._id)}
                            >
                                <CardContent>
                                    <CardActions disableSpacing
                                                 style={{position: 'relative', top: '50%', color: 'white'}}>
                                        <FormGroup>
                                            <FormControlLabel
                                                control={<Checkbox
                                                    checked={selectedCat.includes(category._id)}
                                                    value={category.name}/>}
                                                label={category.name}
                                            />
                                        </FormGroup>
                                    </CardActions>
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Grid>
            }
            <Grid container item xs={12} className="continueButton">
                <Button variant="contained" disabled={selectedCat.length <= 0} color="secondary"
                        onClick={() => handleSubmit()}>Continuar</Button>
            </Grid>

        </div>
    )
}

export default FirstSelect;