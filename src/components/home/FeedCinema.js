import React from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import Typography from "@material-ui/core/Typography/Typography";
import Button from "@material-ui/core/Button/Button";
import imgsTestes from '../../imgs/profile.jpg';
import {Card, CardContent, makeStyles} from "@material-ui/core";
import Avatar from '@material-ui/core/Avatar';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from "@material-ui/core/Hidden/Hidden";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',

        '& > *': {
            margin: 3,
        }
    },
    card: {
        display: 'flex',
        width: 450,
        backgroundColor: 'black',
        color: 'white',
        borderRadius: 7,
        marginRight: '1rem',
    },
    title: {
        fontSize: 16,
        color: '#D10019',
    },
    pos: {
        marginBottom: 12,
    },
    cover: {
        width: 120,
        height: 165,
        zIndex: 500
    },
    small: {
        width: theme.spacing(3),
        height: theme.spacing(3),
    }
}));

function FeedCinema() {
    const classes = useStyles();
    const history = useHistory();

    return (
        <Grid container>
            <Grid container className="p10Top">
                <Grid item container xs={6} alignContent="flex-start">
                    <Typography variant="h6">Cinema</Typography>
                </Grid>
                <Grid xs={6} item container justify="flex-end" alignItems="center">
                    <Button onClick={() => {history.push("/category/Cinema")}}>
                        <Typography variant="caption" className="underlineRed">ver tudo</Typography>
                    </Button>
                </Grid>
            </Grid>
            <Card className={classes.card} variant="outlined">
                <CardContent>
                    <Typography variant="body1" className='redTitles' gutterBottom>
                        Joker
                    </Typography>
                    <Typography variant="h5" component="h2">
                        O filme que é tudo menos uma piada
                    </Typography>
                    <br/>
                    <div className={classes.root}>
                        <Typography title="Foo • Bar • Baz">
                            59 críticas
                        </Typography>
                    </div>
                </CardContent>
                <CardContent>
                    <CardMedia
                        className={classes.cover}
                        image={require ('../../imgs/images.jpg')}
                        title="Live from space album cover"/>
                </CardContent>
            </Card>
            <Hidden mdDown>
                <Card className={classes.card} variant="outlined">
                    <CardContent>
                        <Typography variant="body1" className='redTitles' gutterBottom>
                            Joker
                        </Typography>
                        <Typography variant="h5" component="h2">
                            O filme que é tudo menos uma piada
                        </Typography>
                        <br/>
                        <div className={classes.root}>
                            <Typography title="Foo • Bar • Baz">
                                59 críticas
                            </Typography>
                        </div>
                    </CardContent>
                    <CardContent>
                        <CardMedia
                            className={classes.cover}
                            image={require ('../../imgs/images.jpg')}
                            title="Live from space album cover"/>
                    </CardContent>
                </Card>
            </Hidden>
        </Grid>
    )

}

export default FeedCinema;