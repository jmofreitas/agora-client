import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import {useQuery} from "@apollo/react-hooks";
import {getTopicsByCategory, getTopicsRecOrCat} from "../../queries/queries";
import {useParams} from "react-router-dom";
import {useHistory} from 'react-router-dom';
import LoadingComponent from "../styleComponents/LoadingComponent";
import {Card} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {

        padding: '1rem',
    },
    paper: {
        padding: theme.spacing(0.5),
        margin: 'auto',
        width: '100%',
        marginBottom: '1rem',
    },

    grid: {
        flexWrap: 'noWrap',
        margin: 'auto',
        padding: '1rem',
    }

}));


function FeedLatest(props) {
    const classes = useStyles();
    const history = useHistory();
    const [categoryToUse, setCategoryToUse] = useState('null');
    const {id} = useParams();
    const {loading: loadingCheck, error: errorCheck, data: dataCheck, refetch: refetchTopics} = useQuery(getTopicsByCategory,
        {
            variables: {
                category: categoryToUse
            }
        });

    const makeRequest = () => {
        if(props.model)
            if (props.model.length && id) {
                props.model.map((category) => {
                    if (category.name === id) {
                        setCategoryToUse(category._id)
                    }
                });
            }
    };

    useEffect(() => {
        makeRequest();
    }, [props.model, id]);

    if (loadingCheck) return <LoadingComponent/>;


    return (
        <Grid container className="p10Top">
            {dataCheck && dataCheck.getTopicsByCategory.length > 0 && props.model && dataCheck.getTopicsByCategory.map((topic, key) =>
                (
                <Grid container spacing={1}>
                    <Paper className={classes.paper} onClick={() => {history.push(`/discussion/${topic._id}`)}}>
                        <Grid container spacing={1} className={classes.grid}>
                            <Grid item xs={12} sm container>
                                <Grid item xs container direction="column" spacing={2}>
                                    <Grid item xs>
                                        <Typography gutterBottom variant="caption" color="secondary">
                                            {topic.category[0].name}
                                        </Typography>
                                        <Typography variant="body1" gutterBottom>
                                            {topic.title}
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary">
                                            {topic.upvotes} gostos
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
                )
            )}
        </Grid>
    )
}

export default FeedLatest;