import * as moment from "moment";

function DateParser(date){

    return moment.unix((date)/1000).format("YYYY-MM-DD HH:mm")
}

export default DateParser;